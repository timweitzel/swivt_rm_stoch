function [QD_t] = ImportQDemanddata_v2(delta_T,Tag,Monat,Jahr,duration)
% this function imports Electricity demand data transfers it to an
% array with time stepsize delta_T. Delta_T is measured in minutes. 
% duration in days


file=strcat(pwd,'/90 Input/HeatDemandData/HeatDemand.mat');
load(file)



for d=1:duration
    t=datetime(Jahr,Monat,Tag)+(d-1);
    date=year(t)*10000+month(t)*100+day(t);
    name=strcat('HeatDemand');
    matrix=table2array(eval(name));

    [v,index]=max(matrix(:,4)==date);

    QD_1=matrix(index,5:end);

    N=60/delta_T;

    for i=1:24
        for n=1:N
           QD_t((d-1)*24*N +(i-1)*N+n)=QD_1(i);
        end
    end
end
end