
N=7;

A(1,:)=ImportPricedata_v2(60,1,1,2010,365);
A(2,:)=ImportPricedata_v2(60,1,1,2011,365);
A(3,:)=ImportPricedata_v2(60,1,1,2012,365);
A(4,:)=ImportPricedata_v2(60,1,1,2013,365);
A(5,:)=ImportPricedata_v2(60,1,1,2014,365);
A(6,:)=ImportPricedata_v2(60,1,1,2015,365);
A(7,:)=ImportPricedata_v2(60,1,1,2016,365);


N_vect=[2010; 2011; 2012; 2013; 2014; 2015; 2016];

%% Auswertung
figure(1)
hold on
grid on
for n=1:N
   [h, stats(n)]=cdfplot(A(n,:));
   Minimum(n)=stats(n).min;
   Maximum(n)=stats(n).max;
   Mean(n)=stats(n).mean;
   Median(n)=stats(n).median;
   StdDev(n)=stats(n).std;
end



legend('2010', '2011', '2012', '2013', '2014', '2015', '2016');
xlim([-20 100]);

figure(2)
hold on
grid on

    plot(N_vect, Minimum);
    plot(N_vect, Maximum);
    plot(N_vect, Mean);
    plot(N_vect, Median);
    
legend('Minimum', 'Maximum', 'Mean', 'Median')

figure(3)
hold on
grid on

    plot(N_vect, Mean);
    plot(N_vect, StdDev);
    
legend('Mean', 'StdDev')