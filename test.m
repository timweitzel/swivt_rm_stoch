close all

d=3;
Sim_local.perday(d).scenopt.I.pd_t=Sim_local.perday(d).scenopt.I_TC.pd_t;
Sim_local.perday(d).scenopt.I.ps_t=Sim_local.perday(d).scenopt.I_TC.ps_t;
Sim_local.perday(d).scenopt.I.PV_t_s=Sim_local.perday(d).scenopt.I_TC.PV_t_s;
Sim_local.perday(d).scenopt.I.PDemand_t=Sim_local.perday(d).scenopt.I_TC.PDemand_t;
Sim_local.perday(d).scenopt.I.QDemand_t=Sim_local.perday(d).scenopt.I_TC.QDemand_t;
Sim_local.perday(d).scenopt.I.Pres_t_s=Sim_local.perday(d).scenopt.I_TC.Pres_t_s;

Resultplots_Szen(Sim_local.perday(d).scenopt,1)
Resultplots_Szen(Sim_local.perday(d).scenopt,2)
Resultplots_Szen(Sim_local.perday(d).scenopt,3)
Resultplots_Szen(Sim_local.perday(d).scenopt,4)
Resultplots_Szen(Sim_local.perday(d).scenopt,5)


Sim_local.perday(d).I.pd_t=Sim_local.perday(d).I_TC.pd_t;
Sim_local.perday(d).I.ps_t=Sim_local.perday(d).I_TC.ps_t;
Sim_local.perday(d).I.PV_t_s=Sim_local.perday(d).I_TC.PV_t_s;
Sim_local.perday(d).I.PDemand_t=Sim_local.perday(d).I_TC.PDemand_t;
Sim_local.perday(d).I.QDemand_t=Sim_local.perday(d).I_TC.QDemand_t;
Sim_local.perday(d).I.Pres_t_s=Sim_local.perday(d).I_TC.Pres_t_s;
Resultplots_Szen(Sim_local.perday(d),1)

figure
hold on
grid on

plot(Sim_local.perday(d).scenopt.I.PV_t_s)
plot(Sim_local.perday(d).I.PV_t_s(1:24,1))

A=[-Sim_local.perday(d).I.QDemand_t...
Sim_local.perday(d).res.Qd_CHP_T...
Sim_local.perday(d).res.Qd_HTR_TS...
-Sim_local.perday(d).res.Qd_TESS_TS...
Sim_local.perday(d).res.Qd_THP_TS...
-Sim_local.perday(d).res.Qd_off_in_TS...
-Sim_local.perday(d).res.Qd_off_out_TS];


B=[-Sim_local.perday(d).scenopt.I.QDemand_t...
Sim_local.perday(d).scenopt.res.Qd_CHP_T...
Sim_local.perday(d).scenopt.res.Qd_HTR_TS(:,1)...
-Sim_local.perday(d).scenopt.res.Qd_TESS_TS(:,1)...
Sim_local.perday(d).scenopt.res.Qd_THP_TS(:,1)...
-Sim_local.perday(d).scenopt.res.Qd_off_in_TS(:,1)...
-Sim_local.perday(d).scenopt.res.Qd_off_out_TS(:,1)];