function [ KPI, TIME ] = KPICalc( res, I_TC, I, par, opt, sz_select )
%KPICALC Summary of this function goes here
%   Detailed explanation goes here
TIME.t_date         = I_TC.datetime;
KPI.E_cons          = sum( I_TC.PDemand_t )                 *par.delta_T/60;
KPI.E_PVprod        = sum( I_TC.PV_t_s(:,       sz_select))        *par.delta_T/60;
KPI.E_PVcurt        = sum( res.P_Curt_PV_TS(:,  sz_select))        *par.delta_T/60;
KPI.E_PV2grid       = sum( res.P_PV_g_TS(:,     sz_select))        *par.delta_T/60;
KPI.E_PV2BESS       = sum( res.P_PV_s_TS(:,     sz_select))        *par.delta_T/60;
KPI.E_PV2dem        = sum( res.P_PV_d_TS(:,     sz_select))        *par.delta_T/60;
KPI.E_CHP           = sum( res.P_CHP_T)        *par.delta_T/60;
KPI.E_CHP2grid      = sum( res.P_CHP_g_TS(:,    sz_select))        *par.delta_T/60;
KPI.E_CHP2BESS      = sum( res.P_CHP_s_TS(:,    sz_select))        *par.delta_T/60;
KPI.E_CHP2dem       = sum( res.P_CHP_d_TS(:,    sz_select))        *par.delta_T/60;
KPI.E_THP           = sum( res.P_THP_TS(:,      sz_select))        *par.delta_T/60;
KPI.E_Gridpur       = sum( res.P_Grid_d_T)             *par.delta_T/60;
KPI.E_Gridsold      = sum( res.P_Grid_s_T)             *par.delta_T/60;
KPI.E_ESScrg        = sum( res.P_ESScrg_TS(:,   sz_select))        *par.delta_T/60;
KPI.E_ESSdcrg       = sum( res.P_ESSdcrg_TS(:,  sz_select))        *par.delta_T/60;
KPI.E_ESSlost       = sum( res.E_ESSlosses_TS(:,sz_select))        *par.delta_T/60;

KPI.h_CHP           = sum( res.S_run_CHP_T)             *par.delta_T/60;
KPI.s_CHP           = sum( res.S_sup_CHP_T)           ;
KPI.H_cons          = sum( I_TC.QDemand_t )            *par.delta_T/60;
KPI.H_CHP           = sum( res.Qd_CHP_T)                *par.delta_T/60;
KPI.H_THP           = sum( res.Qd_THP_TS(:,     sz_select))        *par.delta_T/60;
KPI.H_HTR           = sum( res.Qd_HTR_TS(:,     sz_select))        *par.delta_T/60;

Qd_TESS_TS_pos=res.Qd_TESS_TS(res.Qd_TESS_TS>0);
Qd_TESS_TS_neg=res.Qd_TESS_TS(res.Qd_TESS_TS<=0);

KPI.H_TESScrg       = sum( Qd_TESS_TS_pos(:,sz_select))        *par.delta_T/60;
KPI.H_TESSdcrg      = sum( Qd_TESS_TS_neg(:,sz_select))        *par.delta_T/60;

KPI.OF_Cost_Grid_d     = sum( res.C_Grid_d_T);
KPI.OF_Cost_Grid_s     = sum( res.C_Grid_s_T);
KPI.OF_Cost_Grid_Ausgleich   = sum( res.C_Grid_Ausgleich_TS(:,1));
KPI.OF_Cost_Heat_Penalty     = sum( res.C_Heat_Penalty_TS(:,1));
KPI.OF_Cost_Cust       = sum( res.C_Cust_TS(:,  sz_select))        *par.delta_T/60;
KPI.OF_Cost_Custheat   = sum( res.C_Custheat_TS(:,sz_select))        *par.delta_T/60;
KPI.OF_Cost_Prod       = sum( res.C_Prod_TS(:,  sz_select))        *par.delta_T/60;
KPI.OF_Cost_CHP_run    = sum( res.C_run_CHP_T);
KPI.OF_Cost_CHP_sup    = sum( res.C_sup_CHP_T);
KPI.OF_Cost_HTR        = sum( res.C_HTR_TS(:,   sz_select))        *par.delta_T/60;
KPI.OF_Cost_CYC        = sum( res.C_CYC_S(:,    sz_select))        *par.delta_T/60;
KPI.OF_Cost_CAC        = sum( res.C_CAC_TS(:,   sz_select))        *par.delta_T/60;
KPI.OF_Cost_LCC        = KPI.OF_Cost_CAC + KPI.OF_Cost_CYC;

KPI.OF_Cost            = KPI.OF_Cost_Cust +KPI.OF_Cost_Custheat + KPI.OF_Cost_Prod + KPI.OF_Cost_Grid_d + KPI.OF_Cost_Grid_s + KPI.OF_Cost_CHP_run + KPI.OF_Cost_CHP_sup + KPI.OF_Cost_HTR + KPI.OF_Cost_LCC;

KPI.OF_Aut_z          = sum( res.P_Grid_d_T)          *par.delta_T/60;
KPI.OF_Aut_n          = sum( I.Pres_t_s(:,sz_select))            *par.delta_T/60;
KPI.OF_Autarchy       = (1-    KPI.OF_Aut_z  /   KPI.OF_Aut_n )    *   100;

KPI.Tot_CHP2BESS_p100           =   KPI.E_CHP2BESS/KPI.E_CHP*100;
KPI.Tot_CHP2grid_p100           =   KPI.E_CHP2grid/KPI.E_CHP*100;
KPI.Tot_CHP2dem_p100            =   KPI.E_CHP2dem/KPI.E_CHP*100;
KPI.Tot_PV2BESS_p100            =   KPI.E_PV2BESS/KPI.E_PVprod*100;
KPI.Tot_PV2dem_p100            =   KPI.E_PV2dem/KPI.E_PVprod*100;
KPI.Tot_PV2grid_p100            =   KPI.E_PV2grid/KPI.E_PVprod*100;

end

