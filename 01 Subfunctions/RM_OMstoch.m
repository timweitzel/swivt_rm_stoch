function [res end_value x_var cplex opt]=RM_OMstoch(par, I, opt)
% v11b without with power directions
%% Define variables
x_var.subdim={};
x_var.type=[];
x_var.dim.vect=[];
x_var.dim.vect_start=[];
x_var.dim.vect_end=[];
x_var.dim.total=0;
x_var.names=[];
x_var.ub=[];
x_var.lb=[];
big_M=1000000;

x_var   =   createVariable(x_var, 'C_Grid_d',                 par.T,        {'1' 'T' '' '' ''},       'C',    - big_M,  big_M); %Cost for grid demand
x_var   =   createVariable(x_var, 'C_Grid_Ausgleich',       par.T* par.S,	{'2' 'T' 'S' '' ''},       'C',    - big_M*10000,  big_M*10000); %Cost for grid demand
x_var   =   createVariable(x_var, 'C_Heat_Penalty',         par.T* par.S,	{'2' 'T' 'S' '' ''},       'C',    - big_M*10000,  big_M*10000); %Cost for grid demand
x_var   =   createVariable(x_var, 'C_Grid_s',                 par.T,       	{'1' 'T' '' '' ''},       'C',    - big_M,  big_M); %Revenue for grid supply
x_var   =   createVariable(x_var, 'C_Cust',         par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    - big_M,  big_M); %Revenue customer electriciy
x_var   =   createVariable(x_var, 'C_Custheat',     par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    - big_M,  big_M); %Revenue customer heat
x_var   =   createVariable(x_var, 'C_Prod',         par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    - big_M,  big_M); %Cost for locally produced energy
x_var   =   createVariable(x_var, 'C_run_CHP',      par.T,      {'1' 'T' '' '' ''},       'C',    - big_M,  big_M); %Cost for running CHP
x_var   =   createVariable(x_var, 'C_sup_CHP',      par.T,      {'1' 'T' '' '' ''},       'C',    - big_M,  big_M); %Cost for starting CHP
x_var   =   createVariable(x_var, 'C_HTR',          par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    - big_M,  big_M); %Cost Heater
x_var   =   createVariable(x_var, 'C_LCC',          par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    - big_M,  big_M); %BAC
x_var   =   createVariable(x_var, 'C_CYC',          par.S,             {'1' 'S' '' '' ''},       'C',    - big_M,  big_M); %BAC Cyclicyl component
x_var   =   createVariable(x_var, 'C_CAC',          par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    - big_M,  big_M); %BAC calendaric component

x_var   =   createVariable(x_var, 'S_run_CHP',      par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_off_CHP',      par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_sup_CHP',      par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'Qd_CHP',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_CHP',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.CHP.P_max);
x_var   =   createVariable(x_var, 'P_CHP_d',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_CHP_s',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_CHP_t',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_CHP_g',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_CHP_off',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_off_in',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_off_out',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_THP',          par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  par.THP.P_max);



x_var   =   createVariable(x_var, 'Qd_HTR',         par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  par.HTR.Q_dot_max_burner);
x_var   =   createVariable(x_var, 'Qd_THP',         par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_TESS',        par.T* par.S,      {'2' 'T' 'S' '' ''},      'C',    -par.TESS.Qdot_max,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Q_TESS',         par.T_1* par.S,    {'2' 'T_1' 'S' '' ''},   'C',    0,  par.TESS.Q_max);
%x_var   =   createVariable(x_var, 'Qd_delta',       par.T,      {'1' 'T' '' '' ''},      'C',    0, big_M); %Variable wurde entfernt

x_var   =   createVariable(x_var, 'P_ESScrg',       par.T* par.S,      {'2' 'T' 'S' '' ''},        'C',    0,  par.ESS.P_c_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg',      par.T* par.S,      {'2' 'T' 'S' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_d',      par.T* par.S,      {'2' 'T' 'S' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_ESSdcrg_t',      par.T* par.S,      {'2' 'T' 'S' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_ESSdcrg_g',      par.T* par.S,      {'2' 'T' 'S' '' ''},        'C',    0,  big_M); %not allowed to provide to grid
x_var   =   createVariable(x_var, 'S_ESScrg',       par.T* par.S,      {'2' 'T' 'S' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'S_ESSdcrg',      par.T* par.S,      {'2' 'T' 'S' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'S_ESSidle',      par.T* par.S,      {'2' 'T' 'S' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'E_ESSstore',     par.T_1* par.S,    {'2' 'T_1' 'S' '' ''},      'C',    par.ESS.E_min,  par.ESS.E_max);
x_var   =   createVariable(x_var, 'E_ESSlosses',    par.T* par.S,      {'2' 'T' 'S' '' ''},        'C',    0,  big_M);

x_var   =   createVariable(x_var, 'D_DOD',          par.T_C* par.S,              {'2' 'T_C' 'S' '' ''},         'C',   -1,  1);
x_var   =   createVariable(x_var, 'D_DODabs',       par.T_C* par.S,              {'2' 'T_C' 'S' '' ''},         'C',    0,  1);

x_var   =   createVariable(x_var, 'S_SOCMIN',       par.T_C* par.S,              {'2' 'T_C' 'S' '' ''},         'B',    0,  1);
x_var   =   createVariable(x_var, 'S_SOCMAX',       par.T_C* par.S,              {'2' 'T_C' 'S' '' ''},         'B',    0,  1);
x_var   =   createVariable(x_var, 'D_SOCMIN',       par.S,                      {'1' 'S' '' '' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'D_SOCMAX',       par.S,                      {'1' 'S' '' '' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'D_DODper',       par.S,                      {'1' 'S' '' '' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'D_DODabssum',    par.S,                      {'1' 'S' '' '' ''},         'C',    0,  big_M);

x_var   =   createVariable(x_var, 'S_SOCapprox',    par.T * par.SOCApprox * par.S,                       {'3' 'SOCApprox' 'T' 'S' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'S_DODapprox',    par.DODApprox * par.S,                               {'2' 'DODApprox' 'S' '' ''},          'B',    0,  1);
x_var   =   createVariable(x_var, 'S_AhDODapprox',  par.AhDODApprox * par.DODApprox * par.S,             {'3' 'AhDODApprox' 'DODApprox' 'S' ''},       'C',    0,  1);
x_var   =   createVariable(x_var, 'S_CHPP_approx',  par.T * par.n_CHPmodes * par.S,                  {'2' 'n_CHPmodes' 'T' '' ''},       'C',    0,  1);

x_var   =   createVariable(x_var, 'P_Grid_d',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_d_Ausgleich',       par.T*par.S,            {'2' 'T' 'S' '' ''},        'C',    0, big_M ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig

x_var   =   createVariable(x_var, 'P_Grid_d_d',     par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_d_s',     par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_d_t',     par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_s',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_s_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_s_Ausgleich',       par.T*par.S,            {'2' 'T' 'S' '' ''},        'C',    0, big_M ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig

x_var   =   createVariable(x_var, 'S_Grid',         par.T,            {'1' 'T' '' '' ''},        'B',    0, 1); %Selector variable

x_var   =   createVariable(x_var, 'P_Curt_PV',   par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_g',      par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_d',      par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_s',      par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_t',      par.T* par.S,            {'2' 'T' 'S' '' ''},        'C',    0,  big_M);

% Problem ist die Problemvariable fuer CPLEX.
PROBLEM.ub=x_var.ub;
PROBLEM.lb=x_var.lb;
PROBLEM.ctype=x_var.type;
con_count=1;
%% objective Function OF1

[f_sub]=createMatrix( 1 , x_var);

for t=1:par.T
    
    % CHP And exchange with market without scenarios
    
    f_sub.C_Grid_d(1, t)   = 1;
    f_sub.C_Grid_s(1, t)   = 1;
    for al=2:par.n_CHPmodes
        f_sub.S_CHPP_approx(1, (al-1) * par.T + t)     = par.CHP.c_fuel * 1 / par.CHP.eta(al) * par.delta_T/60 * par.CHP.alpha(al) * par.CHP.P * par.CHPmodes(al);
    end
    %f_sub.C_run_CHP(1, t)  = 1;
    f_sub.C_sup_CHP(1, t)  = 1;
    for s=1:par.S
        %Ausgleichsgr??en f?r Constraint verletzungen
        f_sub.C_Heat_Penalty(1,         par.S*(t-1) + s)     = I.Pr_s(s);
        f_sub.C_Grid_Ausgleich(1,       par.S*(t-1) + s)     = I.Pr_s(s);
        %andere Kostenfunktionen
        f_sub.C_Cust(1,                 par.S*(t-1) + s)     = I.Pr_s(s);
        f_sub.C_Custheat(1,             par.S*(t-1) + s)     = I.Pr_s(s);
        f_sub.C_Prod(1,                 par.S*(t-1) + s)     = I.Pr_s(s);
        f_sub.C_HTR(1,                  par.S*(t-1) + s)     = I.Pr_s(s);
        if opt.INCL_LCCinf==0 && opt.INCL_CACinf==I.Pr_s(s)
            for sap=1:par.SOCApprox
                f_sub.S_SOCapprox(1, (sap-1) * par.T * par.S + par.S * (t-1) + s) = par.ESS.LCC.cal_SOC(sap) * par.ESS.E_inv * I.Pr_s(s);
            end
        elseif opt.INCL_LCCinf==1 && opt.INCL_CACinf==1
            for sap=1:par.SOCApprox
                f_sub.S_SOCapprox(1, (sap-1) * par.T * par.S + par.S * (t-1) + s) = par.ESS.LCC.cal_SOC(sap) * par.ESS.E_inv * I.Pr_s(s);
            end
        end
    end
end
for s=1:par.S
    if      opt.INCL_LCCinf==1 && opt.INCL_CACinf==0 %Selector-ansonsten ohne LCC Calculations
        f_sub.C_CYC(1, s)      = I.Pr_s(s);
    elseif opt.INCL_LCCinf==1 && opt.INCL_CACinf==1
        f_sub.C_CYC(1, s)      = I.Pr_s(s);
    end
end

PROBLEM.f = concatenateMatrix(x_var,f_sub);
clearvars f_sub

%% AOF SubOF1
% hier werden die einzelnen Kostenfunktionen ermittelt
[A_eq_AOF_sub, b_eq_AOF]=createMatrix(par.T * 4 + par.S * 1 + par.S * par.T * 7, x_var);
i=1;
for t=1:par.T %Cost for Electricity demand from grid
    A_eq_AOF_sub.P_Grid_d(i, t)    = (I.pd_t(t)+par.Price.total_addon_grid) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Grid_d(i, t)    = -1;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T %Cost for Ausgleichsenergy from grid
    for s=1:par.S
        A_eq_AOF_sub.P_Grid_d_Ausgleich(i, par.S*(t-1) + s)    = par.Price.Ausgleich * par.delta_T/60/1000;
        A_eq_AOF_sub.P_Grid_s_Ausgleich(i, par.S*(t-1) + s)    = par.Price.Ausgleich * par.delta_T/60/1000;
        A_eq_AOF_sub.C_Grid_Ausgleich(i, par.S*(t-1) + s)    = -1;
        i=i+1; con_count=con_count+1;
    end
end
for t=1:par.T %Cost for Ausgleichsenergy from grid
    for s=1:par.S
        A_eq_AOF_sub.Qd_off_in(i,     par.S*(t-1) + s)     = big_M * par.delta_T/60/1000;
        A_eq_AOF_sub.Qd_off_out(i,     par.S*(t-1) + s)     = big_M * par.delta_T/60/1000;
        A_eq_AOF_sub.C_Heat_Penalty(i,     par.S*(t-1) + s)     = -1;
        i=i+1; con_count=con_count+1;
    end
end
for t=1:par.T %Rev for Electricity supply to grid
    A_eq_AOF_sub.P_Grid_s(i, t)    = -I.ps_t(t) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Grid_s(i, t)    = -1;
    i=i+1; con_count=con_count+1;
end

for t=1:par.T %CHP running costs
    for al=2:par.n_CHPmodes
        A_eq_AOF_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHP.c_fuel * 1 / par.CHP.eta(al) * par.delta_T/60 * par.CHP.alpha(al) * par.CHP.P * par.CHPmodes(al);
    end
    A_eq_AOF_sub.C_run_CHP(i, t)   = -1;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T %CHP startup costs
    A_eq_AOF_sub.S_sup_CHP(i, t)   = par.CHP.c_startup ;
    A_eq_AOF_sub.C_sup_CHP(i, t)   = -1;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T %Revenue from customers (electricity)
    for s=1:par.S
        A_eq_AOF_sub.C_Cust(i, par.S*(t-1) + s)    = 1;
        b_eq_AOF(i, 1)    = -par.Price.customer*I.PDemand_t(t)* par.delta_T/60/1000; % hier sp?ter noch szenarien einf?gen
        i=i+1; con_count=con_count+1;
    end
end
for t=1:par.T %Revenue from customers (heat)
    for s=1:par.S
        A_eq_AOF_sub.C_Custheat(i, par.S*(t-1) + s)    = 1;
        b_eq_AOF(i, 1)    = -par.Price.customer_heat*I.QDemand_t(t)* par.delta_T/60/1000; % hier sp?ter noch szenarien einf?gen
        i=i+1; con_count=con_count+1;
    end
end
for t=1:par.T %NC for locally produced energy
    for s=1:par.S
        A_eq_AOF_sub.C_Prod(i, par.S*(t-1) + s)    = 1;
        A_eq_AOF_sub.P_Grid_d(i, t)  = par.Price.total_addon_micro*par.delta_T/60/1000;
        b_eq_AOF(i, 1)               = par.Price.total_addon_micro*I.PDemand_t(t)* par.delta_T/60/1000;
        i=i+1; con_count=con_count+1;
    end
end
for t=1:par.T %Costs for Heater
    for s=1:par.S
        A_eq_AOF_sub.Qd_HTR(i, par.S*(t-1) + s)      = par.HTR.c_fuel * 1 /par.HTR.eta * par.delta_T/60;
        A_eq_AOF_sub.C_HTR(i, par.S*(t-1) + s)       = -1;
        i=i+1; con_count=con_count+1;
    end
end
for t=1:par.T %CAC BAC Battery
    for s=1:par.S
        for sap=1:par.SOCApprox
            A_eq_AOF_sub.S_SOCapprox(i, (sap-1) * par.T * par.S + par.S * (t-1) + s) = par.ESS.LCC.cal_SOC(sap) * par.ESS.E_inv;
        end
        A_eq_AOF_sub.C_CAC(i, par.S*(t-1) + s)       = -1;
        i=i+1; con_count=con_count+1;
    end
end
for s=1:par.S %CYC BAC Battery
    for dap=1:par.DODApprox
        for ahdap=1:par.AhDODApprox
            A_eq_AOF_sub.S_AhDODapprox(i, (ahdap-1)* par.DODApprox * par.S + (dap-1) * par.S +s) = par.ESS.LCC.cyc_DOD(dap,ahdap) * par.ESS.E_inv;
        end
    end
    A_eq_AOF_sub.C_CYC(i, s)       = -1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_AOF_sub, b_eq_AOF, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% Overlay constraint
% in case first optimization defined runtime constraints for CHP plant
if opt.overlay==1
    [A_eq_AOC_sub, b_eq_AOC]=createMatrix(par.T_C * 4 + par.T_C * par.n_CHPmodes, x_var);
    i=1;
    for t=1:par.T_C
        A_eq_AOC_sub.S_run_CHP(i,t)=1;
        b_eq_AOC(i,1)=round(I.scenopt.S_run_CHP_T(t));
        i=i+1; con_count=con_count+1;
        A_eq_AOC_sub.S_off_CHP(i,t)=1;
        b_eq_AOC(i,1)=round(I.scenopt.S_off_CHP_T(t));
        i=i+1; con_count=con_count+1;
        for al=1:par.n_CHPmodes
            A_eq_AOC_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1;
            b_eq_AOC(i,1)=I.scenopt.S_CHPP_approx_n_CHPmodesT(al,t);
            i=i+1; con_count=con_count+1;
        end
        A_eq_AOC_sub.P_Grid_d(i,t)=1;
        b_eq_AOC(i,1)=I.scenopt.P_Grid_d_T(t);
        i=i+1; con_count=con_count+1;
        A_eq_AOC_sub.P_Grid_s(i,t)=1;
        b_eq_AOC(i,1)=I.scenopt.P_Grid_s_T(t);
        i=i+1; con_count=con_count+1;
    end
    PROBLEM = concatCPLEXinput(PROBLEM, A_eq_AOC_sub, b_eq_AOC, x_var, 'eq');
    clearvars -except PROBLEM x_var par I big_M opt con_count
end
%% A2 Energy balance equation
% Energy balance has to be fulfilled at any point in time and in any
% scenario
[A_eq_A2_sub, b_eq_A2]=createMatrix( 8 * par.T * par.S , x_var);
i=1;
for s=1:par.S
    for t=1:par.T %CHP output distribution equation
        for al=1:par.n_CHPmodes
            A_eq_A2_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = -par.CHPmodes(al)*par.CHP.P ;
        end
        A_eq_A2_sub.P_CHP_d(i, par.S * (t-1) + s)  = 1;
        A_eq_A2_sub.P_CHP_s(i, par.S * (t-1) + s)  = 1;
        A_eq_A2_sub.P_CHP_g(i, par.S * (t-1) + s)  = 1;
        if opt.INCL_THP                ==1
            A_eq_A2_sub.P_CHP_t(i, par.S * (t-1) + s)  = 1;
        end
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T %PV output distribution equation
        b_eq_A2(i,1)   = I.PV_t_s(t,s);
        A_eq_A2_sub.P_PV_d(i,       par.S * (t-1) + s)  = 1;
        if opt.INCL_THP                ==1
            A_eq_A2_sub.P_PV_t(i,       par.S * (t-1) + s)  = 1; %to THP
        end
        A_eq_A2_sub.P_PV_s(i,       par.S * (t-1) + s)  = 1;
        A_eq_A2_sub.P_PV_g(i,       par.S * (t-1) + s)  = 1;
        A_eq_A2_sub.P_Curt_PV(i,    par.S * (t-1) + s)   = 1;
        i=i+1; con_count=con_count+1;
    end
    if opt.INCL_THP                ==1
        for t=1:par.T %THP equation
            A_eq_A2_sub.P_CHP_t(i,      par.S * (t-1) + s)    = 1;
            A_eq_A2_sub.P_PV_t(i,       par.S * (t-1) + s)    = 1;
            A_eq_A2_sub.P_Grid_d_t(i,   par.S * (t-1) + s)    = 1;
            A_eq_A2_sub.P_ESSdcrg_t(i,  par.S * (t-1) + s)    = 1;
            A_eq_A2_sub.P_THP(i,        par.S * (t-1) + s)    = -1;
            i=i+1; con_count=con_count+1;
        end
    end
    for t=1:par.T %BESS input
        A_eq_A2_sub.P_CHP_s(i,      par.S * (t-1) + s)  = 1;
        A_eq_A2_sub.P_PV_s(i,       par.S * (t-1) + s)  = 1;
        A_eq_A2_sub.P_Grid_d_s(i,   par.S * (t-1) + s)  = 1;
        A_eq_A2_sub.P_ESScrg(i,     par.S * (t-1) + s)  = -1;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T %BESS dischrage
        A_eq_A2_sub.P_ESSdcrg(i,    par.S * (t-1) + s)    = 1;
        A_eq_A2_sub.P_ESSdcrg_d(i,  par.S * (t-1) + s)    = -1;
        if opt.INCL_THP                ==1
            A_eq_A2_sub.P_ESSdcrg_t(i,  par.S * (t-1) + s)    = -1;
        end
        A_eq_A2_sub.P_ESSdcrg_g(i,  par.S * (t-1) + s)   = -1; %nicht
        %erlaubt, dass BESS and Grid liefert
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T %Demand equatioon / BESS output
        b_eq_A2(i,1)                = I.PDemand_t(t);
        A_eq_A2_sub.P_CHP_d(i,      par.S * (t-1) + s)   = 1;
        A_eq_A2_sub.P_PV_d(i,       par.S * (t-1) + s)   = 1;
        A_eq_A2_sub.P_Grid_d_d(i,   par.S * (t-1) + s)   = 1;
        A_eq_A2_sub.P_ESSdcrg_d(i,  par.S * (t-1) + s)   = 1;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T %Grid equation
        A_eq_A2_sub.P_CHP_g(i,      par.S * (t-1) + s)    = 1;
        A_eq_A2_sub.P_PV_g(i,       par.S * (t-1) + s)    = 1;
        A_eq_A2_sub.P_ESSdcrg_g(i,  par.S * (t-1) + s)    = 1;
        A_eq_A2_sub.P_Grid_s(i,     t)    = -1;
        A_eq_A2_sub.P_Grid_s_Ausgleich(i,  par.S * (t-1) + s)    = -1;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T %Grid demand
        A_eq_A2_sub.P_Grid_d(i,     t)  = 1;
        A_eq_A2_sub.P_Grid_d_Ausgleich(i,  par.S * (t-1) + s)  = 1;
        if opt.INCL_THP                ==1
            A_eq_A2_sub.P_Grid_d_t(i,   par.S * (t-1) + s) = -1;
        end
        A_eq_A2_sub.P_Grid_d_d(i,   par.S * (t-1) + s) = -1;
        A_eq_A2_sub.P_Grid_d_s(i,   par.S * (t-1) + s) = -1;
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_A2_sub, b_eq_A2, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% A3 Energy Storage equation
[A_eq_A3_sub, b_eq_A3]=createMatrix(par.S*(2*par.T+2), x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for s=1:par.S
    for t=1:par.T %hier Standard diskretisierte Speichergleichung
        A_eq_A3_sub.E_ESSstore( i,  par.S * (t-1+1) + s)     = -1;
        A_eq_A3_sub.E_ESSstore( i,  par.S * (t-1) + s)       = (1-par.ESS.sigma);
        A_eq_A3_sub.P_ESScrg( i,    par.S * (t-1) + s)       = (par.ESS.eta_c) * par.delta_T/60;
        A_eq_A3_sub.P_ESSdcrg( i,   par.S * (t-1) + s)       = -(1/par.ESS.eta_d) * par.delta_T/60;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T %Sammelt verluste
        A_eq_A3_sub.E_ESSlosses( i, par.S * (t-1) + s)     = -1;
        A_eq_A3_sub.E_ESSstore( i,  par.S * (t-1) + s)     = (par.ESS.sigma);
        A_eq_A3_sub.P_ESScrg( i,    par.S * (t-1) + s)     = (1-par.ESS.eta_c) * par.delta_T/60;
        A_eq_A3_sub.P_ESSdcrg( i,   par.S * (t-1) + s)     = (1/par.ESS.eta_d-1) * par.delta_T/60;
        i=i+1; con_count=con_count+1;
    end
    A_eq_A3_sub.E_ESSstore(i, 0 + s)        = 1; %Anfangsbedingung
    b_eq_A3 (i,1)                       = I.start.E_ESS_T0;
    i=i+1; con_count=con_count+1;
    
    A_eq_A3_sub.E_ESSstore(i, par.S* par.T + s) = 1; %Endedingung
    b_eq_A3 (i,1)                       = I.end.E_ESS_T0;
    i=i+1; con_count=con_count+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_A3_sub, b_eq_A3, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% A37 A38 Energy Storage constraints
[A_ineq_A37_sub, b_ineq_A37]=createMatrix(6*par.T*par.S, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for s=1:par.S
    for t=1:par.T
        A_ineq_A37_sub.P_ESScrg( i, par.S * (t-1) + s)     = 1;
        A_ineq_A37_sub.S_ESScrg( i, par.S * (t-1) + s)     = -par.ESS.P_c_max;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T
        A_ineq_A37_sub.P_ESScrg( i, par.S * (t-1) + s)     = -1;
        A_ineq_A37_sub.S_ESScrg( i, par.S * (t-1) + s)     = par.ESS.P_c_min;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T
        A_ineq_A37_sub.P_ESSdcrg( i, par.S * (t-1) + s)        = 1;
        A_ineq_A37_sub.S_ESSdcrg( i, par.S * (t-1) + s)        = -par.ESS.P_d_max;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T
        A_ineq_A37_sub.P_ESSdcrg( i, par.S * (t-1) + s)        = -1;
        A_ineq_A37_sub.S_ESSdcrg( i, par.S * (t-1) + s)        = par.ESS.P_d_min;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T
        A_ineq_A37_sub.P_ESSdcrg( i, par.S * (t-1) + s)        = 1;
        A_ineq_A37_sub.S_ESSidle( i, par.S * (t-1) + s)        = par.ESS.P_d_max;
        b_ineq_A37( i, 1)                      = par.ESS.P_d_max;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T
        A_ineq_A37_sub.P_ESScrg( i,  par.S * (t-1) + s)        = 1;
        A_ineq_A37_sub.S_ESSidle( i, par.S * (t-1) + s)        = par.ESS.P_c_max;
        b_ineq_A37( i, 1)                      = par.ESS.P_c_max;
        i=i+1; con_count=con_count+1;
    end
end
% PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_A37_sub, b_ineq_A37, x_var, 'ineq');
clearvars -except PROBLEM x_var par I big_M opt con_count


%% A39 BESS Discharge-Charge-Parity constraint
[A_eq_A39_sub, b_eq_A39]=createMatrix( par.T * par.S , x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for s=1:par.S
    for t=1:par.T
        A_eq_A39_sub.S_ESScrg( i,   par.S *(t-1) +s )        = 1;
        A_eq_A39_sub.S_ESSdcrg( i,  par.S *(t-1) +s )        = 1;
        A_eq_A39_sub.S_ESSidle( i,  par.S *(t-1) +s )        = 1;
        b_eq_A39(i,1)=1;
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_A39_sub, b_eq_A39, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% A4 Heat balance equation
[A_eq_A4_sub, b_eq_A4]=createMatrix(par.T*par.S, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for s=1:par.S
    for t=1:par.T
        A_eq_A4_sub.Qd_HTR( i, par.S *(t-1) +s )           = +1;
        A_eq_A4_sub.Qd_TESS( i, par.S *(t-1) +s )          = -1;
        for al=2:par.n_CHPmodes
            A_eq_A4_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHP.alpha(al)*par.CHP.P*par.CHPmodes(al);
        end
        A_eq_A4_sub.Qd_off_out( i, par.S *(t-1) +s )          = -1;
        A_eq_A4_sub.Qd_off_in( i, par.S *(t-1) +s )           = 1;
        if opt.INCL_THP                ==1
            A_eq_A4_sub.Qd_THP( i, par.S *(t-1) +s )           = +1;
        end
        b_eq_A4(i,1) = I.QDemand_t(t);
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_A4_sub, b_eq_A4, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count


%% THP1 THP equation
if opt.INCL_THP                ==1
    [A_eq_THP_sub, b_eq_THP]=createMatrix(par.T*par.S, x_var);
    i=1;
    for t=1:par.T
        for s=1:par.S
            A_eq_THP_sub.Qd_THP( i, par.S *(t-1) +s )           = +1;
            A_eq_THP_sub.P_THP( i,  par.S *(t-1) +s )           = -par.THP.COP;
            i=i+1; con_count=con_count+1;
        end
    end
else
    [A_eq_THP_sub, b_eq_THP]=createMatrix(par.T*2*par.S, x_var); %in case deselected all equations set to zero.
    i=1;
    for s=1:par.S
        for t=1:par.T
            A_eq_THP_sub.Qd_THP( i, par.S *(t-1) +s )          = +1;
            A_eq_THP_sub.P_THP( i,  par.S *(t-1) +s )           = -par.THP.COP;
            i=i+1; con_count=con_count+1;
        end
        for t=1:par.T
            A_eq_THP_sub.Qd_THP( i, par.S *(t-1) +s )          = +1;
            i=i+1; con_count=con_count+1;
        end
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_THP_sub, b_eq_THP, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% A5 TESS equation
[A_eq_A5_sub, b_eq_A5]=createMatrix( par.S*(par.T+2), x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for s=1:par.S
    for t=1:par.T
        A_eq_A5_sub.Q_TESS( i,  par.S *(t-1+1) +s)          = +1;
        A_eq_A5_sub.Q_TESS( i,  par.S *(t-1) +s)           = -(1-par.TESS.sigma);
        A_eq_A5_sub.Qd_TESS( i,  par.S *(t-1) +s)          = - par.delta_T/60;
        i=i+1; con_count=con_count+1;
    end
    
    A_eq_A5_sub.Q_TESS(i,par.S *0 + s)        = 1; %Anfangsbedingung
    b_eq_A5 (i,1)                       = I.start.Q_TESS_T0;
    i=i+1; con_count=con_count+1;
    
    A_eq_A5_sub.Q_TESS(i,par.S *par.T + s) = 1; %Endedingung
    b_eq_A5 (i,1)                       = I.end.Q_TESS_T0;
    i=i+1; con_count=con_count+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_A5_sub, b_eq_A5, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% A6 CHP Q and P equation
[A_eq_A6_sub, b_eq_A6]=createMatrix(2*par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A6_sub.Qd_CHP( i, t)     = +1;
    %A_eq_A6_sub.P_CHP( i, t)      = - par.CHP.alpha;
    for al=1:par.n_CHPmodes
        A_eq_A6_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = -par.CHP.alpha(al)*par.CHP.P*par.CHPmodes(al);
    end
    i=i+1; con_count=con_count+1;
end
for t=1:par.T %A21
    A_eq_A6_sub.S_off_CHP( i, t)      = +1;
    A_eq_A6_sub.S_run_CHP( i, t)      = +1;
    b_eq_A6( i, 1)                    = +1;
    i=i+1; con_count=con_count+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_A6_sub, b_eq_A6, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% CHP3 CHP P equation
n2=1;
for n3=1:par.CHP.t_min
    n2=n2+n3;
end
[A_ineq_CHP3_sub, b_ineq_CHP3]=createMatrix((par.T-par.CHP.t_min)*par.CHP.t_min+n2, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=par.CHP.t_min+1:par.T
    for tau=1:par.CHP.t_min
        A_ineq_CHP3_sub.S_off_CHP( i, t)            = 1;
        A_ineq_CHP3_sub.S_off_CHP( i, t-1)          = -1;
        A_ineq_CHP3_sub.S_run_CHP( i, t-tau)        = -1;
        i=i+1; con_count=con_count+1;
    end
end
for t=1:par.CHP.t_min
    if I.start.S_run_CHP_T0==1
        delta_t_run=par.CHP.t_min-I.start.S_run_CHP_T0_num; %noch notwendige Laufzeit bevor Abschaltung erfolgen kann. BHKW ist zu diesem Zeitpunkt noch angeschaltet
    else
        delta_t_run=0;
    end
    if t>1
        for tau=1:t-1
            A_ineq_CHP3_sub.S_off_CHP( i, t)        = 1;
            A_ineq_CHP3_sub.S_off_CHP( i, t-1)                  = -1;
            if delta_t_run-t>=0
                A_ineq_CHP3_sub.S_run_CHP( i, t-tau)        = -1;
            else
                b_ineq_CHP3(i,1)    =   0;
            end
            i=i+1; con_count=con_count+1;
        end
    else
        A_ineq_CHP3_sub.S_off_CHP( i, t)        = 1;
        if delta_t_run-t>=0
            b_ineq_CHP3(i,1)        = 1;
        else
            b_ineq_CHP3(i,1)    =   1;
        end
        i=i+1; con_count=con_count+1;
    end
end

PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_CHP3_sub, b_ineq_CHP3, x_var, 'ineq');
clearvars -except PROBLEM x_var par I big_M opt con_count


%% CHP4 Linearization of alpha constraints
[A_eq_CHP4_sub, b_eq_CHP4]=createMatrix(par.T, x_var);
i=1;
for t=1:par.T
    for al=1:par.n_CHPmodes
        A_eq_CHP4_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHPmodes(al)*par.CHP.P ;
    end
    A_eq_CHP4_sub.P_CHP(i,t)                         = -1;
    i=i+1; con_count=con_count+1;
    
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_CHP4_sub, b_eq_CHP4, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count

%% CHP5 Linearization of LCC Constraints weight factors
[A_eq_CHP5_sub, b_eq_CHP5]=createMatrix(5*par.T, x_var);
i=1;
for t=1:par.T
    for al=1:par.n_CHPmodes
        A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1;
    end
    b_eq_CHP5(i,1)                         = 1;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T
    for al=2:par.n_CHPmodes
        A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1;
    end
    A_eq_CHP5_sub.S_run_CHP( i, t)        = -1;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T
    for al=2:par.n_CHPmodes
        A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1;
    end
    A_eq_CHP5_sub.S_off_CHP( i, t)        = 1;
    b_eq_CHP5( i, 1)        = 1;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T
    al=1;
    A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1 ;
    A_eq_CHP5_sub.S_off_CHP( i, t)    = -1;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T
    al=1;
    A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = +1 ;
    A_eq_CHP5_sub.S_run_CHP( i, t)    = 1;
    b_eq_CHP5( i,1)    = 1;
    i=i+1; con_count=con_count+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_CHP5_sub, b_eq_CHP5, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% A20-A22 CHP startup constraints
[A_ineq_A20_sub, b_ineq_A20]=createMatrix( 3*(par.T), x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T %A20
    A_ineq_A20_sub.S_sup_CHP( i, t)      = +1;
    if t==1
        b_ineq_A20( i, 1)      = +1-I.start.S_run_CHP_T0;
    else
        A_ineq_A20_sub.S_run_CHP( i, t-1)      = +1;
        b_ineq_A20( i, 1)      = +1;
    end
    i=i+1; con_count=con_count+1;
end
for t=1:par.T %A21
    A_ineq_A20_sub.S_sup_CHP( i, t)      = +1;
    A_ineq_A20_sub.S_run_CHP( i, t)      = -1;
    b_ineq_A20( i, 1)      = 0;
    i=i+1; con_count=con_count+1;
end
for t=1:par.T %A22
    A_ineq_A20_sub.S_sup_CHP( i, t)      = -1;
    
    A_ineq_A20_sub.S_run_CHP( i, t)      = +1;
    
    if t==1
        b_ineq_A20( i, 1)      = +0+I.start.S_run_CHP_T0;
    else
        A_ineq_A20_sub.S_run_CHP( i, t-1)    = -1;
        b_ineq_A20( i, 1)      = 0;
    end
    i=i+1; con_count=con_count+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_A20_sub, b_ineq_A20, x_var, 'ineq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count


%% A12 PCC constraints with selector
if opt.overlay==0 %otherwise condition is already fulfilled.
    [A_ineq_A12_sub, b_ineq_A12]=createMatrix(2*par.T , x_var);
    i=1;
    for t=1:par.T
        A_ineq_A12_sub.P_Grid_d(i,t)   = 1;
        A_ineq_A12_sub.P_Grid_s(i,t)   = 0;
        A_ineq_A12_sub.S_Grid(i,t)     = -par.Grid.P_d_max;
        b_ineq_A12(i,1) =0;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T
        A_ineq_A12_sub.P_Grid_d(i,t)   = 0;
        A_ineq_A12_sub.P_Grid_s(i,t)   = 1;
        A_ineq_A12_sub.S_Grid(i,t)     = +par.Grid.P_s_max;
        b_ineq_A12(i,1) = par.Grid.P_s_max;
        i=i+1; con_count=con_count+1;
    end
    PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_A12_sub, b_ineq_A12, x_var, 'ineq');
    clearvars -except PROBLEM x_var par I big_M opt con_count con_count
end

%% A47 PV Curtail constraint
[A_ineq_A47_sub, b_ineq_A47]=createMatrix(par.T*par.S , x_var);
i=1;
for s=1:par.S
    for t=1:par.T
        A_ineq_A47_sub.P_Curt_PV(i,par.S*(t-1)+s)   = 1;
        b_ineq_A47(i,1)                 =I.PV_t_s(t,s);
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_A47_sub, b_ineq_A47, x_var, 'ineq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC1 Linearization of LCC
[A_eq_LCC1_sub, b_eq_LCC1]=createMatrix(par.S , x_var);
i=1;
for s=1:par.S
    A_eq_LCC1_sub.D_DODabssum(i,s)     =   -1;
    for ahdap=1:par.AhDODApprox
        for dap=1:par.DODApprox
            A_eq_LCC1_sub.S_AhDODapprox(i, (ahdap-1)* par.DODApprox * par.S + par.S * (dap-1) + s )  = par.AhDODvector(ahdap);
        end
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_LCC1_sub, b_eq_LCC1, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC1a Linearization of LCC
[A_ineq_LCC1a_sub, b_ineq_LCC1a]=createMatrix( par.S * 2 * par.DODApprox  , x_var);
i=1;
for s=1:par.S
    for dap=1:par.DODApprox
        A_ineq_LCC1a_sub.S_DODapprox(i, par.S * (dap-1) +s)     = +big_M;
        A_ineq_LCC1a_sub.D_DODper(i,s)                          = 1;
        b_ineq_LCC1a(i,1)                                       = +big_M+par.DODvector(dap+1);
        i=i+1; con_count=con_count+1;
    end
    %11.2
    for dap=1:par.DODApprox
        A_ineq_LCC1a_sub.S_DODapprox(i,  par.S * (dap-1) +s)    = +par.DODvector(dap);
        A_ineq_LCC1a_sub.D_DODper(i,s)                          = -1;
        b_ineq_LCC1a(i,1)                                       = -0.0001;
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_LCC1a_sub, b_ineq_LCC1a, x_var, 'ineq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC2 Linearization of LCC Constraints weight factors
[A_eq_LCC2_sub, b_eq_LCC2]=createMatrix(par.T * par.S, x_var);
i=1;
for s=1:par.S
    for t=1:par.T
        for sap=1:par.SOCApprox
            A_eq_LCC2_sub.S_SOCapprox(i, (sap-1) * par.T * par.S + (t-1) * par.S + s )     = par.SOCvector(sap);
        end
        A_eq_LCC2_sub.E_ESSstore(i, par.S*(t-1)+s)                         = -1/par.ESS.E_max ;
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_LCC2_sub, b_eq_LCC2, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC2a Linearization of LCC Constraints weight factors

[A_eq_LCC2a_sub, b_eq_LCC2a]=createMatrix(par.T * par.S, x_var);
i=1;
for s=1:par.S
    for t=1:par.T
        for sap=1:par.SOCApprox
            A_eq_LCC2a_sub.S_SOCapprox(i, (sap-1) * par.T * par.S + (t-1) * par.S + s)     = 1;
        end
        b_eq_LCC2a(i,1)                         = 1;
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_LCC2a_sub, b_eq_LCC2a, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC3 DOD Calculations

[A_eq_LCC3_sub, b_eq_LCC3]=createMatrix( par.S*(2*par.T_C+1), x_var);
i=1;
for s=1:par.S
    for t=1:par.T_C
        A_eq_LCC3_sub.D_DOD(i,      (t-1) * par.S + s)      = -1;
        A_eq_LCC3_sub.P_ESScrg(i,   (t-1) * par.S + s)  	= par.ESS.eta_c     *   1/par.ESS.E_max * par.delta_T/60;
        A_eq_LCC3_sub.P_ESSdcrg(i,  (t-1) * par.S + s)      = -1/par.ESS.eta_d  *   1/par.ESS.E_max * par.delta_T/60;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T_C
        A_eq_LCC3_sub.D_DODabs(i,   (t-1) * par.S + s)   = -1;
        A_eq_LCC3_sub.P_ESScrg(i,   (t-1) * par.S + s)   = par.ESS.eta_c     *   1/par.ESS.E_max * par.delta_T/60;
        A_eq_LCC3_sub.P_ESSdcrg(i,  (t-1) * par.S + s)	= 1/par.ESS.eta_d   *   1/par.ESS.E_max * par.delta_T/60;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T_C
        A_eq_LCC3_sub.D_DODabs(i,   (t-1) * par.S + s)       = -1;
        A_eq_LCC3_sub.D_DODabssum(i, s)    =1;
    end
    i=i+1; con_count=con_count+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_LCC3_sub, b_eq_LCC3, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count


%% LCC5 Linearization of LCC
[A_eq_LCC5_sub, b_eq_LCC5]=createMatrix( par.DODApprox * par.S , x_var);
i=1;
for s=1:par.S
    for dap=1:par.DODApprox
        for ahdap=1:par.AhDODApprox
            A_eq_LCC5_sub.S_AhDODapprox(i, (ahdap-1)* par.DODApprox * par.S + (dap-1) * par.S + s) = 1;
        end
        A_eq_LCC5_sub.S_DODapprox(i, (dap-1) * par.S + s) = -1;
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_LCC5_sub, b_eq_LCC5, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC4 Linearization of LCC Binary constraints linearization
[A_eq_LCC4_sub, b_eq_LCC4]=createMatrix( par.S, x_var);
i=1;
for s=1:par.S
    for dap=1:par.DODApprox
        A_eq_LCC4_sub.S_DODapprox(i, (dap-1) * par.S + s ) = 1;
    end
    b_eq_LCC4(i,1)=1;
    i=i+1; con_count=con_count+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_LCC4_sub, b_eq_LCC4, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC20 SOC Min Max identification equations per cycle
[A_ineq_LCC20_sub, b_ineq_LCC20]=createMatrix(4 * par.T_C * par.S, x_var);
i=1;
for s=1:par.S
    for t=1:par.T_C
        A_ineq_LCC20_sub.E_ESSstore(i,  (t-1) * par.S + s)=-1/par.ESS.E;
        A_ineq_LCC20_sub.D_SOCMIN(i,	s)=1;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T_C
        A_ineq_LCC20_sub.E_ESSstore(i,	(t-1) * par.S + s)=1/par.ESS.E;
        A_ineq_LCC20_sub.D_SOCMAX(i,	s)=-1;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T_C
        A_ineq_LCC20_sub.E_ESSstore(i,  (t-1) * par.S + s)=-1/par.ESS.E;
        A_ineq_LCC20_sub.D_SOCMAX(i,	s)=1;
        A_ineq_LCC20_sub.S_SOCMAX(i,	(t-1) * par.S + s)=big_M;
        b_ineq_LCC20(i,1)=big_M;
        i=i+1; con_count=con_count+1;
    end
    for t=1:par.T_C
        A_ineq_LCC20_sub.E_ESSstore(i,	(t-1) * par.S + s)=1/par.ESS.E;
        A_ineq_LCC20_sub.D_SOCMIN(i,	s)=-1;
        A_ineq_LCC20_sub.S_SOCMIN(i,	(t-1) * par.S + s)=big_M;
        b_ineq_LCC20(i,1)=big_M;
        i=i+1; con_count=con_count+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_LCC20_sub, b_ineq_LCC20, x_var, 'ineq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count

%% LCC21 SOC Min Max identification equations per cycle
[A_eq_LCC21_sub, b_eq_LCC21]=createMatrix(3 * par.S, x_var);
i=1;
for s=1:par.S
    for t=1:par.T_C
        A_eq_LCC21_sub.S_SOCMIN(i,	(t-1) * par.S + s )=1;
        b_eq_LCC21(i,1)=1;
    end
    
    i=i+1; con_count=con_count+1;
    for t=1:par.T_C
        A_eq_LCC21_sub.S_SOCMAX(i,  (t-1) * par.S + s )=1;
        b_eq_LCC21(i,1)=1;
    end
    
    i=i+1; con_count=con_count+1;
    A_eq_LCC21_sub.D_DODper(i,  s)   =1;
    A_eq_LCC21_sub.D_SOCMAX(i,  s)=-1;
    A_eq_LCC21_sub.D_SOCMIN(i,  s)=+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_LCC21_sub, b_eq_LCC21, x_var, 'eq');
clearvars -except PROBLEM x_var par I big_M opt con_count con_count


% Optimierung
cplex = Cplex(PROBLEM);

%Add Special order Set for AhDOD approx
SOSnAHDOD=((x_var.details.S_AhDODapprox.vect_end+1)-x_var.details.S_AhDODapprox.vect_start)/par.AhDODApprox;
i=1;
for s=1:par.S
    for dap=1:par.DODApprox
        id_vect=zeros(par.AhDODApprox,1);
        wt_vect=zeros(par.AhDODApprox,1);
        for ahdap=1:par.AhDODApprox
            id_vect(ahdap)=x_var.details.S_AhDODapprox.vect_start-1 + (ahdap-1) * par.DODApprox * par.S + (dap-1) * par.S + s;
            wt_vect(ahdap)=ahdap;
        end
        cplex.addSOSs('2', id_vect, wt_vect, {'sos2ahdod'});
        i=i+1; con_count=con_count+1;
    end
end

%Add Special order set for SOC approx
SOSnDOD=((x_var.details.S_DODapprox.vect_end+1)-x_var.details.S_DODapprox.vect_start)/par.DODApprox;
i=1;

for s=1:par.S
    id_vect=zeros(par.DODApprox,1);
    wt_vect=zeros(par.DODApprox,1);
    for dap=1:par.DODApprox
        id_vect(dap)=x_var.details.S_DODapprox.vect_start-1 + (dap-1) * par.S + s;
        wt_vect(dap)=dap;
    end
    cplex.addSOSs('1', id_vect, wt_vect, {'sos1DOD'});
    i=i+1; con_count=con_count+1;
end


%Add Special order set for SOC approx
SOSnSOC=((x_var.details.S_SOCapprox.vect_end+1)-x_var.details.S_SOCapprox.vect_start)/par.SOCApprox;
i=1;
for s=1:par.S
    for t=1:par.T
        id_vect=zeros(par.SOCApprox,1);
        wt_vect=zeros(par.SOCApprox,1);
        for sap=1:par.SOCApprox
            id_vect(sap)=x_var.details.S_SOCapprox.vect_start-1 + (sap-1)*par.T * par.S + (t-1) * par.S + s;
            wt_vect(sap)=sap;
        end
        cplex.addSOSs('2', id_vect, wt_vect, {'sos2soc'});
        i=i+1; con_count=con_count+1;
    end
end

Alphaap=((x_var.details.S_CHPP_approx.vect_end+1)-x_var.details.S_CHPP_approx.vect_start)/par.n_CHPmodes;
i=1;
for t=1:par.T
    id_vect=zeros(par.n_CHPmodes,1);
    wt_vect=zeros(par.n_CHPmodes,1);
    for j=1:par.n_CHPmodes
        id_vect(j)=x_var.details.S_CHPP_approx.vect_start-1 + (j-1)*par.T +t;
        wt_vect(j)=j;
    end
    cplex.addSOSs('2', id_vect, wt_vect, {'Alphaap2'});
    i=i+1; con_count=con_count+1;
end

if opt.cplex.DisplayIter==0
    cplex.DisplayFunc=[];
end
if opt.cplex.TimeCap>0
    cplex.Param.timelimit.Cur=opt.cplex.TimeCap;
end
if opt.cplex.SolTolerance>0
    cplex.Param.mip.tolerances.mipgap.Cur=opt.cplex.SolTolerance;
end

sol_found=0;
cplex.solve;

%% output back to other format

x           =cplex.Solution.x;
[res end_value] =divide_xvar_RM(x,  x_var.dim.vect, x_var.names, x_var.subdim ,  par);

res.time    =cplex.Solution.time;
res.mipgap  =cplex.Solution.miprelgap;


end