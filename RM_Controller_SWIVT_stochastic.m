handles=findall(0,'type','figure');
handles.delete;
%clear all

folderold=pwd;
%folder1='/Users/Timm/Documents/MATLABgit/Results/'; %for Apple
folder1=strcat('C:\Users\weitzel\Desktop\local\'); % Herakeles
cd(folder1);
foldername=strcat('Simulation-RMSToch-', datestr(now,'_yy-mm-dd_HH-MM'));
mkdir(foldername);
folder=strcat(folder1,foldername);
cd(folderold);

%% Parameterdefinitionen (Basis)
[par, opt] = RM_Parameter_Definition;

%%Create Scenario Data
for d=1:par.T_total
    T_0=par.T_O;
    if par.T_O_Master>par.T_total-d+1 %Anpassung der Optimierungszeitr?ume, so dass am Ender der Laufzeit nur noch ein Tag optimiret wird
        T_0     = par.T_total-d+1;
    end
    [PVDATA.perday(d).PV_t_s, PVDATA.perday(d).Pr_s, PVDATA.perday(d).PVforecast_t, PVDATA.perday(d).PVobserved_t]       = PVScenarioGen(par.S_org, par.S, month(par.sdate.datetime(1)+(d-1)), day(par.sdate.datetime(1)+(d-1)), par.delta_T, T_0);
    disp(sprintf(strcat('Scenarios for day ',num2str(d),' of ', num2str(par.T_total),' generated')))
end


%% Optimierung
tges=datetime('now');
% Initialize Simvariables
Sol.Sim.KPI=[]; Sol.Sim.res=[]; Sol.Sim.I=[]; Sol.Sim.perday=[]; Sol.Sim.par=[]; Sol.par=par; Sol.opt=opt; Sol.PVDATA=[];
% Define Parameter variations
for p1v=1:par.parvar(1).n_max
    for p2v=1:par.parvar(2).n_max
        for p3v=1:par.parvar(3).n_max
            for p4v=1:par.parvar(4).n_max
                for p5v=1:par.parvar(5).n_max
                    for p6v=1:par.parvar(6).n_max
                        for period=1:par.periods
                            for eps=1:par.eps_const.eps_max
                                p6max=par.parvar(6).n_max;
                                p56max=p6max*par.parvar(5).n_max;
                                p46max=p56max*par.parvar(4).n_max;
                                p36max=p46max*par.parvar(3).n_max;
                                p26max=p36max*par.parvar(2).n_max;
                                p16max=p26max*par.parvar(1).n_max;
                                
                                pv=(p1v-1)*p26max + (p2v-1) * p36max + (p3v-1)*p46max +(p4v-1)*p56max + (p5v-1)*p6max + p6v;
                                
                                p=floor(pv/p16max*100);
                                
                                p_mult =1; %Multiplikator f?r Preise aus 2015
                                
                                % ?bergabe Optimierungsparameter
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt  = opt;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).I=[];
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).pv=[];
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).PVDATA=PVDATA;
                                Sol.pv(pv).ID=pv;
                                
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par  = par;
                                
                                % Parametervariation
                                if opt.INCL_parvar_ScenOpt==1
                                    Sol.Sim(eps,period,p1v,p2v).opt.scenopt=  par.parvar(1).vect(p1v);
                                    if opt.INCL_parvar_Ausgleichspreis==1
                                        Sol.Sim(eps,period,p1v,p2v).par.Price.Ausgleich=  par.parvar(2).vect(p2v);
                                    end
                                    if opt.INCL_parvar_ESSSize==1
                                        Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E           =  par.parvar(3).vect(p3v);
                                        Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E_max       =  par.parvar(3).vect(p3v);
                                        Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.P_c_max     =  par.parvar(3).vect(p3v)*3;
                                        Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.P_d_max     =  par.parvar(3).vect(p3v)*3;
                                    end
                                    
                                elseif  opt.INCL_parvar_LCConoff==1
                                    Sol.Sim(eps,period,1).opt.INCL_LCCinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_LCCinf             =0;
                                elseif opt.INCL_parvar_CAConoff==1
                                    Sol.Sim(eps,period,1).opt.INCL_CACinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_CACinf             =0;
                                elseif opt.INCL_parvar_LCCCAConoff==1
                                    Sol.Sim(eps,period,1).opt.INCL_LCCinf             =1;
                                    Sol.Sim(eps,period,1).opt.INCL_CACinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_LCCinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_CACinf             =0;
                                    Sol.Sim(eps,period,3).opt.INCL_LCCinf             =0;
                                    Sol.Sim(eps,period,3).opt.INCL_CACinf             =1;
                                    Sol.Sim(eps,period,3).par.CYC=48;
                                    Sol.Sim(eps,period,4).opt.INCL_LCCinf             =0;
                                    Sol.Sim(eps,period,4).opt.INCL_CACinf             =0;
                                    Sol.Sim(eps,period,4).par.CYC=48;
                                    
                                elseif opt.INCL_parvar_Scenarios==1
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E           =  par.parvar(2).vect(p2v);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E_max       =  par.parvar(2).vect(p2v);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.P_c_max     =  par.parvar(2).vect(p2v)*3;
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.P_d_max     =  par.parvar(2).vect(p2v)*3;
                                    if par.parvar(2).vect(p2v)==0
                                        Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt.INCL_LCCinf             =0;
                                        Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt.INCL_CACinf             =0;
                                        Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt.INCL_LCC                =0;
                                    end
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.price       =  par.parvar(1).vect(p1v);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E_inv       =  par.parvar(1).vect(p1v) * par.parvar(2).vect(p2v);
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.P           =  par.parvar(4).vect(p4v,1);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.P_max       =  par.parvar(4).vect(p4v,1);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.P_min       =  par.parvar(4).vect(p4v,1)*0.5;
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.alpha       =   par.parvar(4).CHP(p4v).alpha;
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.eta         =   par.parvar(4).CHP(p4v).eta;
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.TESS.Q_max      = par.parvar(4).vect(p4v,1)* par.parvar(4).CHP(p4v).alpha(length(par.CHPmodes))*par.parvar(3).vect(p3v) * 50 * 1.163 * 30/1000;
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.Price.pmult     =  par.parvar(6).vect(p6v);
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.Price.total_addon_micro= par.Price.total_addon_grid - par.parvar(5).vect(p5v);
                                    
                                    Sol.pv(pv).ID=pv;
                                    Sol.pv(pv).ESSprice     =par.parvar(1).vect(p1v);
                                    Sol.pv(pv).ESSpriceID   =p1v;
                                    Sol.pv(pv).ESSsize      =par.parvar(2).vect(p2v);
                                    Sol.pv(pv).ESSsizeID    =p2v;
                                    Sol.pv(pv).TESSsize     =par.parvar(3).vect(p3v);
                                    Sol.pv(pv).TESSsizeID   =p3v;
                                    Sol.pv(pv).CHPsize      =par.parvar(4).vect(p4v);
                                    Sol.pv(pv).CHPsizeID    =p4v;
                                    Sol.pv(pv).NWC          =par.parvar(5).vect(p5v);
                                    Sol.pv(pv).NWCID        =p5v;
                                    Sol.pv(pv).pmult        =par.parvar(6).vect(p6v);
                                    Sol.pv(pv).pmultID      =p6v;
                                    
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

% parvar counter
p6max=par.parvar(6).n_max;
p56max=p6max*par.parvar(5).n_max;
p46max=p56max*par.parvar(4).n_max;
p36max=p46max*par.parvar(3).n_max;
p26max=p36max*par.parvar(2).n_max;
p16max=p26max*par.parvar(1).n_max;

for p1v=1:par.parvar(1).n_max   %parfor kann hier vor die entsprechende Erste Instanz gesetzt werden die parallelisiert werden kann.
    for p2v=1:par.parvar(2).n_max
        for p3v=1:par.parvar(3).n_max
            for p4v=1:par.parvar(4).n_max
                for p5v=1:par.parvar(5).n_max
                    for p6v=1:par.parvar(6).n_max
                        for period=1:par.periods
                            for eps=1:par.eps_const.eps_max
                                Sim_local=Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v);
                                pv=(p1v-1)*p26max + (p2v-1) * p36max + (p3v-1)*p46max +(p4v-1)*p56max + (p5v-1)*p6max + p6v;
                                Sim_local.ID=pv;
                                Sim_local.pv=Sol.pv(pv);
                                for d=1:par.T_total
                                    Sim_local.perday(d).t_sim.start=datetime('now');
                                    if d==1
                                        Sim_local.TIME.t_sim_start=Sim_local.perday(d).t_sim.start;
                                    end
                                    
                                    Sim_local = RM_Dailyoptimization(Sim_local, d, par, opt, period, PVDATA.perday(d));
                                    
                                    if d==1
                                        Sim_local.KPI  = Sim_local.perday(d).KPI;
                                        Sim_local.TIME.t_start  = Sim_local.perday(d).I_TC.datetime;
                                        Sim_local.res  = Sim_local.perday(d).res;
                                        Sim_local.I    = Sim_local.perday(d).I_TC;
                                        
                                        Sim_local.scenopt.KPI  = Sim_local.perday(d).scenopt.KPI;
                                        Sim_local.scenopt.res  = Sim_local.perday(d).scenopt.res;
                                        Sim_local.scenopt.I    = Sim_local.perday(d).scenopt.I_TC;
                                    else
                                        Sim_local.TIME.t_end  = Sim_local.perday(d).I_TC.datetime;
                                        [Sim_local]             =concatenateResults_RM_simplified(Sim_local,           Sim_local.perday(d));
                                        [Sim_local.scenopt]     =concatenateResults_RM_simplified(Sim_local.scenopt,   Sim_local.perday(d).scenopt);
                                        
                                    end
                                    
                                    
                                    if d==par.T_total
                                        Sim_local.res.E_ESSstore_T_1S(par.T_total*24*par.delta_T/60,:)    =   Sim_local.perday(d).end.E_ESSstore_T_1S;
                                        Sim_local.res.Q_TESS_T_1S(par.T_total*24*par.delta_T/60,:)        =   Sim_local.perday(d).end.Q_TESS_T_1S;
                                    end
                                    
                                    sim_t            = Sim_local.perday(d).t_sim.end-Sim_local.perday(d).t_sim.start;
                                    sim_t2           = Sim_local.perday(d).t_sim.end-tges;
                                    sim_status       = Sim_local.perday(d).t_sim.status;
                                    sim_gap          = Sim_local.perday(d).res.mipgap;
                                    sim_gap_scen     = Sim_local.perday(d).scenopt.res.mipgap;
                                    
                                    S=sprintf(strcat('ParVar: ', num2str(pv),'/',num2str( p16max),...
                                        ' -- Period: ', num2str(period),'/',num2str(par.periods),...
                                        ' -- Eps: ', num2str(eps), '/', num2str(par.eps_const.eps_max),...
                                        ' -- Day: ', num2str(d), '/', num2str(par.T_total),...
                                        ' -- t_iter: ', datestr(sim_t,'HH:MM:SS'),...
                                        ' -- t_ges: ', datestr(sim_t2,'dd:HH:MM:SS'),...
                                        ' -- gap: ', num2str( ceil(sim_gap*1000)/10 ),' %%',...
                                        ' -- gapscenopt: ', num2str( ceil(sim_gap_scen*1000)/10 ),' %%',...
                                        ' -- status: ', sim_status));
                                    disp(S);
                                    
                                end
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).ID=pv;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par=Sim_local.par;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI=Sim_local.KPI;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt=Sim_local.opt;
                                
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_BatteryLifetimePrediction         =   Sim_local.par.ESS.E_inv/Sim_local.KPI.OF_Cost_LCC *Sim_local.par.T_total/365;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_BatteryAvgCycDay        =   Sim_local.KPI.E_ESScrg *Sim_local.par.ESS.eta_c / Sim_local.par.ESS.E /Sim_local.par.T_total;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_BatteryAvgSOC_p100      =    sum( Sim_local.res.E_ESSstore_T_1S(:,1)) / (Sim_local.par.T_total*24*par.delta_T/60) /Sim_local.par.ESS.E*100;    
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_Profit                  =   -Sim_local.KPI.OF_Cost;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_GridPurchase_p100       =    Sim_local.KPI.E_Gridpur/(Sim_local.KPI.E_cons + Sim_local.KPI.E_ESSlost)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_GridSold_p100           =    Sim_local.KPI.E_Gridsold/(Sim_local.KPI.E_CHP + Sim_local.KPI.E_PVprod)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_heatshare_p100      =    Sim_local.KPI.H_CHP/(Sim_local.KPI.H_cons + Sim_local.KPI.H_TESScrg + Sim_local.KPI.H_TESSdcrg)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_THP_heatshare_p100      =    Sim_local.KPI.H_THP/(Sim_local.KPI.H_cons + Sim_local.KPI.H_TESScrg + Sim_local.KPI.H_TESSdcrg)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_HTR_heatshare_p100      =    Sim_local.KPI.H_HTR/(Sim_local.KPI.H_cons + Sim_local.KPI.H_TESScrg + Sim_local.KPI.H_TESSdcrg)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_TESSAvgCycDay           =    (Sim_local.KPI.H_TESScrg - Sim_local.KPI.H_TESSdcrg) /2 / Sim_local.par.TESS.Q_max /Sim_local.par.T_total;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_TESSAvgSOC_p100         =    sum( Sim_local.res.Q_TESS_T_1S(:,1)) / (Sim_local.par.T_total*24*par.delta_T/60) /Sim_local.par.TESS.Q_max*100;    
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_AvgP_p100           =    Sim_local.KPI.E_CHP/Sim_local.KPI.h_CHP/Sim_local.par.CHP.P*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_h_to_sup          	=    Sim_local.KPI.h_CHP/Sim_local.KPI.s_CHP;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_h                   =    Sim_local.KPI.h_CHP;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP2BESS_p100           =   Sim_local.KPI.E_CHP2BESS/Sim_local.KPI.E_CHP*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP2grid_p100           =   Sim_local.KPI.E_CHP2grid/Sim_local.KPI.E_CHP*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP2dem_p100            =   Sim_local.KPI.E_CHP2dem/Sim_local.KPI.E_CHP*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_PV2BESS_p100            =   Sim_local.KPI.E_PV2BESS/Sim_local.KPI.E_PVprod*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_PV2dem_p100             =   Sim_local.KPI.E_PV2dem/Sim_local.KPI.E_PVprod*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_PV2grid_p100            =   Sim_local.KPI.E_PV2grid/Sim_local.KPI.E_PVprod*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).TIME.t_sim_start                =    Sim_local.TIME.t_sim_start;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).TIME.t_sim_end                  =    Sim_local.TIME.t_sim_end;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.t_sim_delta                 =    Sim_local.TIME.t_sim_end-Sim_local.TIME.t_sim_start;

%                                 
                                cd(folder);
                                name=strcat('Sim','_', num2str(pv), '_', num2str(p16max));
                                eval(sprintf(strcat(name,' =Sim_local;')));
                                save(strcat(name,'.mat'),name);
                                save('Sol.mat','Sol');
                                writexlsx(Sol, folder, pv);
                                cd(folderold);

                            end
                        end
                    end
                end
            end
        end
    end
end

 cd(folder);
 name='Sol';
 save(strcat(name,'.mat'),name);
 name2=strcat('Sim_1_',num2str(p16max));
 load(name2);
 eval(sprintf(strcat('Sim1 = ',name2,';')));
 writexlsx(Sol, folder, p16max);
cd(folderold);

clearvars -except Sol Sim1

Resultplots_Szen(Sim1,1);



