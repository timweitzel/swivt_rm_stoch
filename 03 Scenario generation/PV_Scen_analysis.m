
function [PV_s_t, PV_f, PV_o] = PVScenarioGen(n_Sz, n_SZ_red_max,month, day)
close all
clear all


date=datetime(2015,1,1);
%Load real PV data
for i=1:1:365
   PV(i,:)=ImportPVdata(60,day(date+i-1),month(date+i-1),year(date+1-1),1,1);
end

start       = 3;
samplesize  = 365-start;
date        = datetime(2015,1,1)+start-1;
n_day       = days(datetime(2015,month,day)-date);

for i=1:1:365-start
   PV_o(i,:)=ImportPVdata(60,day(date+i-1),month(date+i-1),year(date+1-1),1,1);

   PV_f(i,:)=mean(PV(i:i+start-1,:));
   mult=sum(PV_o(i,:))/sum(PV_f(i,:));
   PV_f(i,:)= PV_f(i,:)*mult;
end
PV_d=PV_o-PV_f;


PVmax=max(max(max(PV_o)),max(max(PV_f)));
PV_f_norm=PV_f/PVmax;
PV_o_norm=PV_o/PVmax;
PV_f_norm_v=reshape(PV_f_norm', [samplesize*24 ,1]);
PV_o_norm_v=reshape(PV_o_norm', [samplesize*24 ,1]);
PV_f_v=reshape(PV_f', [samplesize*24 ,1]);
PV_o_v=reshape(PV_o', [samplesize*24 ,1]);
PV_d_v=reshape(PV_d', [samplesize*24 ,1]);
PV_d_norm_v=PV_f_norm_v-PV_o_norm_v;
PV_d_norm=PV_f_norm-PV_o_norm;

 X=[-80:0.1:80];
%% empirical distribution function
bounds=[0, 0.5, 2.5, 5:5:95, 120];
n_K=numel(bounds)-1;
%figure 
%hold on
for k=1:n_K
    lb=bounds(k);
    ub=bounds(k+1);
    [f,x]=ecdf(PV_d_v(and(PV_o_v>=lb,PV_o_v<=ub)));
    F(:,k)=LinearizeECDF(f,x,X);
    %plot(x,f)
end

%% Error Covariance
PV_f_norm_select=PV_f_norm(n_day,:);
PV_f_select=PV_f(n_day,:);
PV_o_select=PV_o(n_day,:);

% Bin selection for seleted forecast
K_vect=zeros(24,1);
for i=1:24
    for k=1:n_K    
        lb=bounds(k);
        ub=bounds(k+1);
        if PV_f_select(i)>=lb && PV_f_select(i)<=ub
           K_vect(i)=k; 
        end
    end
end

%n_SZ=500; defined in input

mu=zeros(n_SZ,24);
%sigma=cov(PV_d(200:250,:));
sigma=eye(24);
Z=mvnrnd(mu,sigma);
%Zp=mvncdf(Z,mu,sigma);
Zp=cdf('Normal',Z,0,1);

%% Generate scenarios
for sz=1:n_SZ
     PV_d_select_szen(sz,:) = InverseFunction( F, X, K_vect, Zp(sz,:));
     PV_f_select_scen(sz,:)=-PV_d_select_szen(sz,:)+PV_f_select;
end

% plot
% figure
% plot(PV_f_select)
% hold on
% plot(min(PV_f_select_scen))
% plot(max(PV_f_select_scen))
% plot(PV_o_select)
% 
% figure
% plot(PV_f_select)
% hold on
% plot(PV_f_select_scen')
% 
% 
% figure
% plot (PV_d(220,:))

%% Scenario reduction 
% Heuristic Implemented based on Li et al. 2016 - A Scenario Optimal Reduction Method for Wind Power Time Series
PV_sz   =PV_f_select_scen;

P_sz    =ones(n_SZ,1)/n_SZ; % Wahrsheinlichkeiten der einzelnen Szenarios

sz      =round(rand(1)*n_SZ); %random scenario selection

%Step 1
PV_sz_rem   = PV_sz([1:sz-1, sz+1:end],:);
P_sz_rem    = P_sz([1:sz-1, sz+1:end],1);

n_SZ_rem    = n_SZ-1;
P_sz_red    = 1;
n_SZ_red    = 1;
P_sz_red_old    = P_sz(sz);
PV_sz_red   = PV_sz(sz,:);

Q=4;
eps_d   =0.5; %urspr?nglich 0.15
eps_m   =0.5;%urspr?nglich 0.1
eps_s   =0.01;


f=9999;
f_d=99999;
n_count=1;
while f(n_count)>eps_m && f_d(n_count)>eps_d
    
    if n_count==n_SZ_red_max
        break
    end
        n_count=n_count+1;
    %Step 2
    [ f(n_count) ] = ScenRedMomDistance( Q, P_sz, PV_sz, P_sz_red, PV_sz_red );
    [ f_d(n_count) ] = ScenRedSpaceDistance( PV_sz_red, P_sz_rem, PV_sz_rem );
    if f(n_count)<=eps_m && f_d(n_count)<=eps_d
        break
    end
    %Step 3
    sz      =round(rand(1)*(n_SZ_rem-1)+1);
    %select random scenario from remaining data
    PV_sz_sel   = PV_sz_rem(sz,:);
    P_sz_sel    = P_sz_rem(sz,1);
    PV_sz_rem   = PV_sz_rem([1:sz-1, sz+1:end],:);
    P_sz_rem    = P_sz_rem([1:sz-1, sz+1:end],1);
    n_SZ_rem    = n_SZ_rem-1;
    
    for n=1:n_SZ_red
        c_dist(n)    =   P_sz_sel*pdist([PV_sz_sel;PV_sz_red(n,:)],'euclidean');
    end
    c=min(c_dist);
    clear c_dist
    
    if c>= eps_s
        PV_sz_red(n_SZ_red+1,:)   = PV_sz_sel;
        P_sz_red_old(n_SZ_red+1)=P_sz_sel;
        n_SZ_red    = n_SZ_red+1;
        for n=1:n_SZ_red
            P_sz_red(n)    = P_sz_red_old(n)/sum( P_sz_red_old); %hier wieder auf gleichverteilung normiert.
        end
    else
        for n=1:n_SZ_red
            c_dist(n)    =   pdist([PV_sz_sel;PV_sz_red(n,:)],'euclidean');
        end
        CN=[1:n_SZ_red];
        sz_min=CN(c_dist==min(c_dist));
        clear c_dist
        P_sz_red_old(sz_min)= P_sz_red_old(sz_min)+P_sz_sel;
        for n=1:n_SZ_red
            P_sz_red(n)    = P_sz_red_old(n)/sum( P_sz_red_old); %hier wieder auf gleichverteilung normiert.
        end
    end

end
