folderold=pwd;
foldername=strcat('Simulation-RMSToch-_17-08-19_16-22');
folder=strcat(pwd,'/',foldername);
cd(folder);
load('Sim_1_8'); load('Sim_2_8'); load('Sim_3_8'); load('Sim_4_8'); load('Sim_5_8'); load('Sim_6_8'); load('Sim_7_8'); load('Sim_8_8');
%1_8 ist die variante durch Scenopt, Sim5_8 die Variante unter normaler
%optimierung. Beide zeigen die Observed values der PV. Sim_2_8 bis Sim_4_8 sind Varianten mit Ausgleichsenergiepreisen von 300, 200 und 100 EUR/kWh 
 %load('Sim_1_8'); load('Sim_5_8'); %1_2 ist die variante durch Scenopt, Sim2_2 die Variante unter normaler optimierung. Beide zeigen die Observed values.
cd(folderold);

Resultplots_Szen(Sim_1_8,1)
Resultplots_Szen(Sim_5_8,1)

%% Plot PV Data
Sim=Sim_1_8;
par=Sim.par;
n_D     =Sim.par.T_total; %Anzahl Tage die Simuliert wurden
n_SZ    =Sim.par.S; %Anzahl Szenarien

PV_t_s=[]; PVforecast_t=[]; PVobserved_t =[];
for d=1:n_D
    PV_t_s          =[PV_t_s', Sim.PVDATA.perday(d).PV_t_s(1:24,:)']';
    PVforecast_t    =[PVforecast_t', Sim.PVDATA.perday(d).PVforecast_t(1:24,1)']';
    PVobserved_t    =[PVobserved_t', Sim.PVDATA.perday(d).PVobserved_t(1:24,1)']'; 
end

figure
subplot(3,1,1)
grid on
hold on
stairs(PVforecast_t);
stairs(PVobserved_t);
stairs(min(PV_t_s')');
stairs(max(PV_t_s')');

legend('PV-forecast','PV-observed','min of all Scenarios','max of all Scenarios', 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(3,1,2)
title('Prognoseabweichung einzelner Scenarien in kw')
stairs(PV_t_s-repmat(PVforecast_t,1,n_SZ));
grid on
hold on
name=[];
for sz=1:n_SZ
    name=[name {strcat('PV-Sz# ', num2str(sz))}];
end
legend(name, 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(3,1,3)
title('Prognoseabweichung Messdaten in kw')
grid on
hold on
stairs(PVobserved_t-PVforecast_t);
stairs(min((PV_t_s-repmat(PVforecast_t,1,n_SZ))')');
stairs(max((PV_t_s-repmat(PVforecast_t,1,n_SZ))')');



legend('Observed', 'Min all Scenarios', 'Max all Scenarios', 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

%% Auswertung ?ber Storage verlauf

figure
subplot(5,1,1)
grid on
hold on
stairs(PVforecast_t);
stairs(PVobserved_t);
legend('PV-forecast','PV-observed', 'Location','north','Orientation','horizontal')
ylabel('in kW');
xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])


subplot(5,1,2)
hold all;
grid on;
title('Energy exchanged with the grid')
stairs(Sim_1_8.res.P_Grid_d_T-Sim_1_8.res.P_Grid_s_T);
stairs(Sim_5_8.res.P_Grid_d_T-Sim_5_8.res.P_Grid_s_T);
stairs(Sim_2_8.res.P_Grid_d_T-Sim_2_8.res.P_Grid_s_T);
%stairs(I.Pres_t_s(:,sz));

legend('Scenario optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,3)
hold all;
grid on;
title('ben?tigte Auschleichsleistung')
stairs(Sim_1_8.res.P_Grid_d_Ausgleich_TS(:,1)-Sim_1_8.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(Sim_5_8.res.P_Grid_d_Ausgleich_TS(:,1)-Sim_5_8.res.P_Grid_s_Ausgleich_TS(:,1));
stairs(Sim_2_8.res.P_Grid_d_Ausgleich_TS(:,1)-Sim_2_8.res.P_Grid_s_Ausgleich_TS(:,1));

legend('Scenario optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,4)
hold all;
grid on;
title('PV Curtailment')

stairs(Sim_1_8.res.P_Curt_PV_TS(:,1));
stairs(Sim_5_8.res.P_Curt_PV_TS(:,1));
stairs(Sim_2_8.res.P_Curt_PV_TS(:,1));

legend('Scenario optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
ylabel('in kW');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

subplot(5,1,5)
hold all;
grid on;

title('Energy stored')
ylabel('in kWh');
ylim([0 par.ESS.E_max]);
plot(Sim_1_8.res.E_ESSstore_T_1S(:,1),'r-');
plot(Sim_5_8.res.E_ESSstore_T_1S(:,1),'b-');
plot(Sim_2_8.res.E_ESSstore_T_1S(:,1),'m-');

xlim([0 24*60/par.delta_T*par.T_total]);
set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
set(gca,'XTickLabel', [0:12:24*par.T_total])

legend('Scenario optimized', 'no uncertainty', 'Scenario optimized - Ausgleichspreis 300');
