function y=charvect(dimension, char) %returns a simple vector of chars in the dimension given
 for int=1:dimension
    y(int)=char; 
 end
end