function [x_res, x_res_names, x_res_adv]=divide_x(x,dim_x_vect, x_names, x_subdim, M, I, J, T, T_1, K, CYCLES, DODApprox, SOCApprox)
% ?berf?hrt Vecktor x in ein lesbares Format
l=length(dim_x_vect);
x_lb=1;
x_ub=0;
for i=1:l
    if i>1
        x_lb=x_lb+dim_x_vect(i-1);
    end
    x_ub=x_ub+dim_x_vect(i);
    
    local=x(x_lb:x_ub);
    x_res.(sprintf(strcat('v', num2str(i))))=local; 
    x_res_names.(sprintf(strcat(x_names{i})))=local;
    
    num=eval(x_subdim{i,1});
    switch num
        
    case 0
        x_res_adv.(sprintf(strcat(x_names{i})))=local;
    
    case 1
        var1=eval(x_subdim{i,2});
        x_res_adv.(sprintf(strcat(x_names{i},'_', x_subdim{i,2})))=local;
        
    
    case 2
        var1=eval(x_subdim{i,2});
        var2=eval(x_subdim{i,3});
        
       local2=zeros(var1,var2);
        for v1=1:var1
            for v2=1:var2
                        local2(v1,v2)=local( (v1-1)*var2 + v2 );
            end
        end
         x_res_adv.(sprintf(strcat(x_names{i},'_', x_subdim{i,2}, x_subdim{i,3} )))=local2;

    case 3
        var1=eval(x_subdim{i,2});
        var2=eval(x_subdim{i,3});
        var3=eval(x_subdim{i,4});
        local2=zeros(var1,var2,var3);
        for v1=1:var1
            for v2=1:var2
                for v3=1:var3
                        local2(v1,v2,v3)=local( (v1-1)*var2*var3 + (v2-1)*var3+ v3 );
                end
            end
        end
        x_res_adv.(sprintf(strcat(x_names{i},'_', x_subdim{i,2}, x_subdim{i,3}, x_subdim{i,4} )))=local2;

            
    case 4
        var1=eval(x_subdim{i,2});
        var2=eval(x_subdim{i,3});
        var3=eval(x_subdim{i,4});
        var4=eval(x_subdim{i,5});
        local2=zeros(var1,var2,var3,var4);
        for v1=1:var1
            for v2=1:var2
                for v3=1:var3
                    for v4=1:var4
                        local2(v1,v2,v3,v4)=local( (v1-1)*var2*var3*var4 + (v2-1)*var3*var4 + (v3-1)*var4 + v4 );
                    end
                end
            end
        end
        x_res_adv.(sprintf(strcat(x_names{i},'_', x_subdim{i,2}, x_subdim{i,3}, x_subdim{i,4},  x_subdim{i,5} )))=local2;
        
    otherwise
            disp ('other value');
            
        
    end

    
end




end
