clear all
handles=findall(0,'type','figure');
handles.delete;


E_max=10;%in kwh
n_100=6000;

DODdelta=0.001;
DOD=[0.05:DODdelta:1.00];%[0:par.DODdelta:1];%
AhDODdelta=0.01;
AhDOD=[0:AhDODdelta:5];%
C_BATT=2.3;%Ah
n_DOD=numel(DOD);
n_Ah=numel(AhDOD);


damage=-ones(n_DOD,n_Ah);
num=-ones(n_DOD,n_Ah);

for ndod=1:n_DOD
    for nah=1:n_Ah
            [damage(ndod,nah), ncyc(ndod,nah)]=BA_cyc_Sarasketa_v2(DOD(ndod), AhDOD(nah)*C_BATT, 0.2);
    end
end



%% Linarized model

DODdeltaA=0.01;
DODA=[0.05:DODdeltaA:1.00];%[0:par.DODdelta:1];%
AhDODdeltaA=5;
AhDODA=[0:AhDODdeltaA:5];%

n_DA=length(DODA);
n_AhA=length(AhDODA);

damage2=-ones(n_DA,n_AhA);
for ndod=1:n_DOD
    for nda=1:n_DA-1
        if DOD(ndod)>= DODA(nda) && DOD(ndod)< DODA(nda+1)
            for nah=1:n_Ah
                for naha=1:n_AhA-1
                        if AhDOD(nah)> AhDODA(naha) && AhDOD(nah)<= AhDODA(naha+1)
                            w=(AhDOD(nah)- AhDODA(naha+1))/(AhDODA(naha)-AhDODA(naha+1));
                            damage2(ndod,nah) = w * BA_cyc_Sarasketa_v2(DODA(nda+1),AhDODA(naha)*C_BATT,0.2) + (1-w) * BA_cyc_Sarasketa_v2(DODA(nda+1),AhDODA(naha+1)*C_BATT,0.2);
                        end
                end
            end
        elseif  DOD(ndod) == DODA(nda+1) && nda==n_DA-1
            for nah=1:n_Ah
                for naha=1:n_AhA-1
                        if AhDOD(nah)> AhDODA(naha) && AhDOD(nah)<= AhDODA(naha+1)
                            w=(AhDOD(nah)- AhDODA(naha+1))/(AhDODA(naha)-AhDODA(naha+1));
                            damage2(ndod,nah) = w * BA_cyc_Sarasketa_v2(DODA(nda+1), AhDODA(naha)*C_BATT,0.2) + (1-w) * BA_cyc_Sarasketa_v2(DODA(nda+1), AhDODA(naha+1)*C_BATT,0.2);
                        end
                end
            end
            
        end
    end

end

%% Delta calculation

delta=damage-damage2;
num=0;
E=0;
SE=0;
max_d=damage(1,1);
min_d=damage(1,1);
for ndod=1:n_DOD
    for nah=1:n_Ah
           E=E+delta(ndod,nah);
           SE=SE+delta(ndod,nah)^2;
           num=num+1;
           max_d=max(max_d, damage(ndod,nah));
           min_d=min(min_d, damage(ndod,nah));
    end
end
RMSE=sqrt(SE/num);
NRMSE=RMSE/(max_d-min_d);
ME=E/num;




%% Plots
figure(1)
hold on
grid on

n=find(AhDOD==1);
plot(DOD*100,ncyc(:,n));

xl2=xlabel('DOD [in %]');
xlim([0 100]);
yl2=ylabel('\number of cycles');
yl2.FontSize=12;
xl2.FontSize=12;
%ylim([0 0.03]);
%ylim([0 10^6]);

set(gcf,'Units','centimeters','PaperPosition',[0 0 12 5],'PaperPositionMode','manual','PaperSize',[12 5])
print(gcf,'-r300','-djpeg', '-opengl','/Users/Timm/Dropbox/Promotion/BA_cyc_cyclenumber')


%% plot 2
figure (2)
subplot (1,2,1)
hold on;
grid on;

name={};
symbol={'^', 'v', '>', '<', 'd', 's', 'x', '*', 'o', '+'};
for i=1:4
    n=find(DOD==floor((0.2+(i-1)*0.2)*10)/10);
    p(i)      =plot(AhDOD,damage(n,:)*100,'-');
    name{i}=strcat('\DOD = ',' ', num2str((0.2+(i-1)*0.2)*100),' %');
end


for i=1:4
    n=find(DOD==floor((0.2+(i-1)*0.2)*10)/10);
    p(i+4)=     plot(AhDOD,damage2(n,:)*100,'--');
    name{i+4}=  strcat('\DOD = ',' ', num2str((0.2+(i-1)*0.2)*100),' % (linearized)');
end
xl1=xlabel('Ah/C_{Batt}');
xlim([0 5]);
yl1=ylabel('\epsilon_{cyc} [in % ]');
yl1.FontSize=12;
xl1.FontSize=12;
ylim([0 0.03]);
legend(p,name,'Location', 'Northwest')


subplot (1,2,2)

hold on;
grid on;

n=find(AhDOD==1);
plot(DOD*100,damage(:,n)*100);
plot(DOD*100,damage2(:,n)*100,'--');

xl2=xlabel('DOD [in %]');
xlim([0 100]);
yl2=ylabel('\epsilon_{cyc} [in %]');
yl2.FontSize=12;
xl2.FontSize=12;
ylim([0 0.03]);
lgd2=legend('model (Ah/C_{Batt}= 1)', 'linearized (Ah/C_{Batt}= 1)' );
set(lgd2, 'Location', 'Northwest')

set(gcf,'Units','centimeters','PaperPosition',[0 0 12 5],'PaperPositionMode','manual','PaperSize',[12 5])
print(gcf,'-r300','-djpeg', '-opengl','/Users/Timm/Dropbox/Promotion/BA_cyc_linearization')



