function [p_aging, n, Ahmax, Qloss] = BA_cyc_Sarasketa_v2(DOD1, Ah, C_verl, CR)
% taken from Suri and onori 2016 for LiFEPo4 battery

if nargin ==3
    CR=1;
end
a1=  -0.14;
a2=  -0.08;
a3=  +1.92;
a4=  -2.51;
b1=  +0.48;
b2=  -2.42;
b3=  +1.57;

k=0.017;
z=0.8;

DOD=DOD1*100;
if DOD1==0
    p_aging=0;
    n=99999;
    Ahmax=0;
    Qloss=0;
else
    factor= (a1+a2*DOD+a3*DOD^0.5+a4*log(DOD))*(b1*CR^2+b2*CR+b3) *k^z;
    Ahmax=(C_verl*100/factor)^(1/z);
    Qloss=factor*Ah^z;
    p_aging=Ah/Ahmax;
    if p_aging==0
        n=999999;
    else
        n=1/p_aging;
    end
end

end