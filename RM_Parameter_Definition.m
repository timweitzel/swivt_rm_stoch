function [ par, opt ] = RM_Parameter_Definition
%RM_PARAMETER_DEFINITION Summary of this function goes here

%CHP Plant sollte mindestens 5000 h im Jahr laufen und nicht k?rzer als 1h
%am St?ck laufen
par.CHP.c_fuel = 0.04; %4 ct / kWh HO
par.CHP.c_startup = 0; % [EUR] -- (1/3) einer Periode kann nur die Halbe Leistung verwendet werden (approximiert)
% ==> c_fuel * P_CHP * (1/3) * 2
par.CHP.alpha = [9999; 2.720; 2.107; 1.975];%[999999; 999999; 2.720; 2.107; 1.975]; % bei der Erzeugung von 1kW elektrische Leistung werden alpha kW thermische Leistung durch Abw??rme erzeugt
par.CHPmodes=[0; 0.50; 0.75; 1.0];%[0; 0.499999; 0.50; 0.75; 1.0];%2.720; 2.107; 1.975]; %Percent of rated electrical power
par.CHP.eta = [0.00001; 0.699; 0.613; 0.613];%;[0.0002; 0.0002; 0.699; 0.611; 0.613]; % W?rmewirkungsgrad
par.n_CHPmodes=length(par.CHPmodes);
% Laut Viessmann-Website sind typische Werte alpha = 1.5 ... 2
% entspricht REZIPROKE STROMKENNZAHL: 0.5
par.CHP.P = 20;
par.CHP.P_max = par.CHP.P;
par.CHP.P_min = par.CHP.P*0.5;% [kW] Betriebspunkt 50/81 als andere Variante
par.CHP.t_min = 1 + 3;


%Energy Storage
%Battery is a LiFePo4 battery
par.ESS.cell.C_batt=2.3; %2.3 Ah per cell
par.ESS.cell.U_n=3.3; %3.3V nominal voltage
par.ESS.cell.E=par.ESS.cell.C_batt * par.ESS.cell.U_n; %7.59W per Cell
par.ESS.cell.n=14000; % in Summe ca 53,13kWh bei 7000 Zellen

par.ESS.sigma=83.333e-6; % entspricht 0.2\% pro Tag, mittlerer Wert in Chen et. al (2009)
%sigma_bat = 0.000027778; %sigma_SD = 0.00067; % pro Stunde; entspricht 2% self-discharge / Monat
%sigma_bat = 0.000125 % entspricht 0.3\% pro Tag, max. Wert in  Chen et. al (2009)
par.ESS.RTE=0.95;
par.ESS.eta_c=sqrt(par.ESS.RTE);
par.ESS.eta_d=sqrt(par.ESS.RTE);

par.ESS.E=100;%par.ESS.cell.E*par.ESS.cell.n/1000;
par.ESS.E_max=par.ESS.E;
par.ESS.E_min=0;
par.ESS.P_c_max=par.ESS.E*3;
par.ESS.P_d_max=par.ESS.E*3;
par.ESS.P_c_min=1; %minimum damit neuer Zyklus beginnt
par.ESS.P_d_min=1; %minimum damit neuer Zyklus beginnt
par.ESS.P_min=0;
par.ESS.price           = 150;
par.ESS.CAC.temp        = 25;
par.ESS.CAC.C_verl      = 0.2;
par.ESS.CAC.max_year    = 20;
par.ESS.CYC.n_max       = 10000;
par.ESS.E_inv=par.ESS.price * par.ESS.E;

% Heater
par.HTR.c_fuel=0.055; % Preis ist leicht h?her, da in KWK Betrieb teilweise Kosten wieder zur?ckgeholt werden k?nnen
par.HTR.Q_dot_max_burner = 300;
par.HTR.eta= 0.92*0.89; % 0.84Brennwert- (und nicht Heizwert-) -basierter Wirkungsgrad

% Thermal Heat pump
opt.INCL_THP   	 =0; %on/off switchfor THP
par.THP.P_max=150; %3}50kw Pumpen max elektrische kW
par.THP.COP= 0.99; %COP
opt.THP_COP_var =0; % 1-THP varoable COP/ 0-electric Heater or THP with constant COP

% Thermischer Energiespeichers
par.TESS.sigma= 0.0062; % 0.62% pro Stunde Verlust -- W???rme soll zwei Tage vorhalten
par.TESS.eta_c= 1;
par.TESS.eta_d= 1;
par.TESS.Q_max= par.CHP.P*par.CHP.alpha(numel(par.CHP.alpha)) * 50 * 1.163 * 30/1000;%50l/kw thermisch installiert * 1,163 Wh/kgK * Delta T=30K
par.TESS.Qdot_max= 100000000000;

% Netzgrenzen
par.Grid.P_d_max=1000;
par.Grid.P_s_max=1000;

% Preisinformationen

par.Price.pmult=1; %multiplier for given market prices

par.Price.Ausgleich=     9999999; %EUR/MWh

par.Price.customer=     250; %EUR/MWh
par.Price.customer_heat=     65; %EUR/MWh
par.Price.EEG_umlage=   63.54; %EUR/MWh
par.Price.Grid_AP=      55.00; %EUR/MWh
par.Price.Grid_GP=      2.78; %EUR/MWh
par.Price.sonstige=     16.36; %EUR/MWh
par.Price.Stromsteuer=  29.12; %EUR/MWh
par.Price.Konzession=   15.90; %EUR/MWh
par.Price.Grid_Sell=   80; %EUR/MWh 110 in SWIVT Case
par.Price.total_addon_micro= par.Price.EEG_umlage + par.Price.sonstige + par.Price.Grid_GP;
par.Price.total_addon_grid = par.Price.total_addon_micro + par.Price.Grid_AP + par.Price.Konzession + par.Price.Stromsteuer;
par.Price.NC_delta=par.Price.total_addon_grid-par.Price.total_addon_micro;

%LCC calculations
par.DODdelta=0.1;
par.DODvector=[0; 0.05; 0.15; 0.25; 0.35; 0.45; 0.55; 0.65; 0.75; 0.85; 0.95; 1.00];%[0:par.DODdelta:1];%
par.AhDODdelta=0.1;
par.AhDODvector=[0; 10];%
par.SOCdelta=0.1;
par.SOCvector=[0; 0.15; 0.30; 0.45; 0.60; 0.75; 0.90; 1.00];%0.00 ; 0.05; 0.10; 0.20; 0.30; 0.40; 0.45; 0.55; 0.60; 0.70; 0.80; 0.90; 0.95; 1.00];

par.DODApprox=length(par.DODvector)-1;%number of linearizations for DOD and SOC
par.SOCApprox=length(par.SOCvector);%number of linearizations for DOD and SOC
par.AhDODApprox=length(par.AhDODvector);



%% Options for optimization
opt.INCL_LCCinf             =1; %=1 falls LCC in Zielfunktion aktiv geschaltet werden sollen
opt.INCL_LCC                =1;
opt.INCL_CACinf             =1; %=1 falls CAC in Zielfunktion aufgenommen werden sollen

opt.cplex.DisplayIter       =0;% 1-Iteration werden angezeigt, 0- werden nicht angezeigt %
opt.cplex.TimeCap           =20*60;%5*60min TimeCap
opt.cplex.SolTolerance      =0.005;%0.005;
opt.INCL_epsconstraint      =0;
opt.Multiple_Periods        =0; %Multiple period analysis


opt.save_conc_res           =1; %save sub results
opt.save_cplex              =1; %1: Cplex objekte werden gespeichert

opt.scenopt =1; %Scenario Optimization engaged


%% Kontrollparameter NB

%Epsilon constraint Bedingungen
par.eps_const.delta     =0.1;
par.eps_const.vector    =[0:0.1:1];%[-0.3:par.eps_const.delta:1];
par.eps_const.num       =numel(par.eps_const.vector);

if opt.INCL_epsconstraint==1
    par.eps_const.eps_max=par.eps_const.num-1;
else
    par.eps_const.eps_max=1;
    par.eps_const.num=2;
end


% Anzahl der perioden
par.delta_T     =  60; %min
par.T_O_Master  =  2; %Optimierungszeitraum in Tagen
par.T_O         =  par.T_O_Master;
par.T           =  24/(par.delta_T/60)*par.T_O_Master; %Optimierungszeitraum in Zeiteinheiten delta_T
par.T_1         =  par.T + 1;
par.T_C         =  24/(par.delta_T/60);
par.T_total     =  16; %kompletter Betrachtungszeitraum in TAgen

% Anzahl der kalkulierten Szenarioen
par.S_org       =  100; %Urspur?ngliche Anzahl
par.S           =  20; %reduzierte Anzahl

if opt.Multiple_Periods==0
    par.periods=1;
    par.sdate.datetime(1) = datetime(2015, 6, 1); %17.7.
else
    par.sdate.datetime(1) = datetime(2015, 1, 12); %jeweils Montag als Anfangsdatum
    par.sdate.datetime(2) = datetime(2015, 4, 13);
    par.sdate.datetime(3) = datetime(2015, 7, 13);
    par.sdate.datetime(4) = datetime(2015, 10, 12);
    par.periods=numel(par.sdate.datetime);
end


%Parametervarition


% Parameter Variation - only select one option!
opt.INCL_parvar_ESSprice    =0;
opt.INCL_parvar_LCConoff    =0;
opt.INCL_parvar_CAConoff    =0;
opt.INCL_parvar_LCCCAConoff =0;
opt.INCL_parvar_Scenarios   =0;

opt.INCL_parvar_ScenOpt         =1;
opt.INCL_parvar_Ausgleichspreis =1;
opt.INCL_parvar_ESSSize         =1;


for i=1:6
    par.parvar(i).vect=[];
    par.parvar(i).name='';
    par.parvar(i).ID=i;
    par.parvar(i).n_max=1;
end

if opt.INCL_parvar_ScenOpt==1
    par.parvar(1).ID=1;
    par.parvar(1).name='OPTscenOpt';
    par.parvar(1).vect=[1, 0];
    par.parvar(1).n_max=numel(par.parvar(1).vect);
    if opt.INCL_parvar_ESSSize==1
        par.parvar(3).ID=3;
        par.parvar(3).name='ESSsize';
        par.parvar(3).vect=[200 400];
        par.parvar(3).n_max=numel(par.parvar(3).vect);
    end
    if opt.INCL_parvar_Ausgleichspreis==1
        par.parvar(2).ID=2;
        par.parvar(2).name='Ausgleichsenergiepreis';
        par.parvar(2).vect=[999999 300 200];
        par.parvar(2).n_max=numel(par.parvar(2).vect);  
    end
elseif opt.INCL_parvar_ESSprice==1
    par.parvar(1).ID=1;
    par.parvar(1).name='ESSprice';
    par.parvar(1).vect=[200, 250, 300, 350, 400, 450, 500];%,150,200,250,300,400,500,600,1000]; %Unterschiedliche Preise f?r Speicher
    par.parvar(1).n_max=numel(par.parvar(1).vect);
    
elseif opt.INCL_parvar_Scenarios==1
    par.parvar(1).ID=1;
    par.parvar(1).name='ESSprice';
    par.parvar(1).vect=[100 200 300 500];
    par.parvar(1).n_max=numel(par.parvar(1).vect);
    
    par.parvar(2).ID=2;
    par.parvar(2).name='ESSsize';
    par.parvar(2).vect=[200 300 400];
    par.parvar(2).n_max=numel(par.parvar(2).vect);
    
    par.parvar(3).ID=3;
    par.parvar(3).name='TESSsize_mult';
    par.parvar(3).vect=[1, 2];
    par.parvar(3).n_max=numel(par.parvar(3).vect);
    
    par.parvar(4).ID=4;
    par.parvar(4).name='CHPsize';
    par.parvar(4).vect=[20; 50; 70; 140]; %(variant, Pmax)
    par.parvar(4).n_max=length(par.parvar(4).vect);
    
    par.parvar(4).CHP(1).alpha=[9999; 2.720; 2.107; 1.975];
    par.parvar(4).CHP(1).eta=[0.00001; 0.699; 0.613; 0.613];    
    par.parvar(4).CHP(2).alpha=[9999; 1.840; 1.684; 1.620];
    par.parvar(4).CHP(2).eta=[0.00001; 0.535; 0.542; 0.559];    
    par.parvar(4).CHP(3).alpha=[9999; 1.886; 1.604; 1.643];
    par.parvar(4).CHP(3).eta=[0.00001; 0.541; 0.535; 0.564];
    par.parvar(4).CHP(4).alpha=[9999; 1.857; 1.629; 1.479];
    par.parvar(4).CHP(4).eta=[0.00001; 0.573; 0.55; 0.539];
    
    par.parvar(5).ID=5;
    par.parvar(5).name='Delta_NetworkCharges';
    par.parvar(5).vect=[par.Price.NC_delta, 125, par.Price.total_addon_grid];
    par.parvar(5).n_max=numel(par.parvar(5).vect);
    
    par.parvar(6).ID=6;
    par.parvar(6).name='Increase_GridPrice';
    par.parvar(6).vect=[1];
    par.parvar(6).n_max=numel(par.parvar(6).vect);
    
    
elseif  opt.INCL_parvar_LCConoff==1
    par.parvar(1).n_max=2;
elseif  opt.INCL_parvar_CAConoff==1
    par.parvar(1).n_max=2;
elseif  opt.INCL_parvar_LCCCAConoff==1
    par.parvar(1).n_max=4;
else
    par.parvar(1).n_max=1;
end

%LCC Parameter
par.ESS.LCC.cyc_DOD=zeros(par.DODApprox,1);
for dprox=1:par.DODApprox
    for ahprox=1:par.AhDODApprox
        par.ESS.LCC.cyc_DOD(dprox,ahprox)= BA_cyc_Sarasketa_v2(     par.DODvector(dprox+1),    par.AhDODvector(ahprox)*par.ESS.cell.C_batt,    par.ESS.CAC.C_verl);
    end
end
for sprox=1:par.SOCApprox
    par.ESS.LCC.cal_SOC(sprox)= BA_cal_Sarasketa_v1(     (par.SOCvector(sprox)),    par.ESS.CAC.temp , par.delta_T,par.ESS.CAC.C_verl ,par.ESS.CAC.max_year);
end
clear dprox sprox ahprox



end

